#pragma once
#include "Tokenizer.h"
#include <fstream>
#include <cstdlib>
#ifdef VS_COMPILER
#include <filesystem>
#else
#include <boost\filesystem.hpp>
#endif

class sf
{
	public:
		sf();
		sf(char * fname);
		std::vector<std::string> m_in;
		std::vector<std::string> m_out;
		std::vector<std::string> inSF; //����� ������
		std::vector<std::string> outSF; // ����� �������
		std::string name;
		void ReadSf(char * fname);
		void SaveSf(char * fname, std::vector<std::string> &functions);
};