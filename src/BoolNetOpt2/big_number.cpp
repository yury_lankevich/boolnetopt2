#include "big_number.h"

big_number::big_number()
{
	n = 0;
}
/*
bool big_number::is_neighbor(const big_number & target) const
{
	bool found_one_missmatch = false;
	unsigned and = 0;
	for (int i = n >> 4; i >= 0; i--)
	{
		and = number[i] & target.number[i];
		if (((and << 1) | 0x55555555 | and) != 0xffffffff)
		{
			unsigned int mask = 0x3;
			while (mask != 0)
			{
				if ( (mask & and) != mask)
				{
					if (found_one_missmatch) return false;
					found_one_missmatch = true;
				}
				mask = mask << 2;
			}
		}
	}
	return true;
}
*/
big_number::big_number(std::string &str)
{
	n =(unsigned int) str.length() - 1;
	unsigned int dw_number = (unsigned int) (str.length()+15) / 16;
	number.resize(dw_number);
	unsigned int dw_cnt = 0;
	unsigned char code_cnt = 0; // 'b01 : 0
	                            // 'b10 : 1
	                            // 'b11 : -
	                            // 'b00 : not used
	for( auto i = str.begin(); i != str.end(); i++)	{
		//TODO: error if it is not 1,0 or -
		number[dw_cnt] |= ((*i)=='0' ? 0x1 : ( (*i)=='1' ? 0x2 : 0x3)) << (code_cnt << 1);
		if(code_cnt >= 15)	{
			code_cnt = 0;
			dw_cnt++;
			if(dw_cnt == dw_number) break;
			number[dw_cnt] = 0;
		}
		else
			code_cnt++;
	}
}
void big_number::exclude_pos(unsigned int num)
{
	/*if(num > n)	{
		printf("Error (big_number): get_code (num).... num > max_num");
		exit(1);
	}*/
	unsigned int current_dw = num >> 4;// num / 16
	{
		unsigned int mask = 0xffffffff<<(num<<1);
		number[current_dw] = ((number[current_dw]>>2) & mask) | ((~mask) & number[current_dw]);
	}
	for(++current_dw ;current_dw < number.size(); current_dw++)	{
		number[current_dw-1] |= (number[current_dw] << 30);
		//number[current_dw] >>= 2;
		number[current_dw] = (number[current_dw] >> 2);
	}
	if(n)	n--;
	unsigned int new_size = (n >> 4) + 1;
	if (new_size < number.size())
		number.erase(number.begin() + new_size);
	//else
	//	number.clear();
}
char big_number::get_code(unsigned int num) const
{
	/*if(num > n)	{
		printf("Error (big_number): get_code (num).... num > max_num");
		exit(1);
	}*/
	unsigned int current_dw = num >> 4;
	return (number[current_dw] >> ((num % 16)<<1 )) & 0x3;
}

void big_number::set_code(unsigned int num, char code)
{
	if (num > n)	{
		printf("Error (big_number): get_code (num).... num > max_num");
		exit(1);
	}
	unsigned int current_dw = num >> 4;
	number[current_dw] = number[current_dw] & (0xffffffff ^ (0x3 << ((num % 16) << 1)));
	number[current_dw] = number[current_dw] | (code << ((num % 16) << 1));
}

bool big_number::glew(const big_number& target, big_number& result, bool & partially) const
{
	result.number.resize( (n >> 4) + 1);
	result.n = n;
	big_number result_p;
	result_p.number.resize((n >> 4) + 1);
	result_p.n = n;
	bool extends = false, is_extended = false;
	bool this_unchanged, target_unchanged;
	bool diference = false;
	//bool this_extends =false;
	unsigned int and_ = 0;
	partially = false;
	if(n != target.n)	{
		printf("Error(big_number) : different size numbers are compared ");
		exit(1);
	}
	for(int i = n >> 4; i >= 0; i--)	{
		and_ = number[i] & target.number[i];
		this_unchanged = (and_ == number[i]);
		target_unchanged = (and_ == target.number[i]);
		if( this_unchanged & target_unchanged) // if( true & true)
		{
			result.number[i] = and_;
			result_p.number[i] = and_;
			continue;
		}
		if(diference) return false;
		if(this_unchanged == target_unchanged ) { // if( false == false)
			partially |= (extends | is_extended);
			//todo binary search
			unsigned int mask = 0xffffffff, tmp_mask_left, tmp_mask_right;
			char shift = 16;
			if(n < 2) shift = 2;
			else
				if(n < 4) shift = 4;
				else if(n<8) shift = 8;
				else shift = 16;
			mask = mask>>( (16-shift)<<1);
			bool right_eq, left_eq;
			while(shift > 1) { //�.�. ��� �� 2 ����
				tmp_mask_right = (mask >> shift) & mask; //0x0000ffff
				right_eq = ( (tmp_mask_right & number[i]) == (tmp_mask_right & target.number[i]) );
				tmp_mask_left = (mask << shift) & mask; //0xffff0000
				left_eq = ( (tmp_mask_left & number[i]) == (tmp_mask_left & target.number[i]) );
				if(left_eq == right_eq) // false == false : (������� (true == true) ���� �� �����)
				{
					//return false;
					and_ = (number[i] & target.number[i]);

					//��������� ������ ��������
					this_unchanged = !((and_ ^ number[i]) & tmp_mask_right);          //and == number[i]
					target_unchanged = !((and_ ^ target.number[i]) & tmp_mask_right); //and == taget.number[i]
						if(extends && this_unchanged) return false;
						if(is_extended && target_unchanged) return false;
						extends |= target_unchanged;
						is_extended |= this_unchanged;
					bool right_extended = this_unchanged | target_unchanged;
					//��������� ����� ��������
					this_unchanged = !((and_ ^ number[i]) & tmp_mask_left); //and == number[i]
					target_unchanged = !((and_ ^ target.number[i]) & tmp_mask_left);
						if(extends && this_unchanged) return false;
						if( is_extended && target_unchanged) return false;
						extends |= target_unchanged;
						is_extended |= this_unchanged;
					bool left_extended = this_unchanged | target_unchanged;
					if(right_extended == left_extended) return false; //false == false or true == true
					if(right_extended) right_eq = true;
					partially = true;
					//TODO: ���� left or right exdends or extended, ����� ��� extended the difference �������� �� -
				}
				mask = right_eq ? tmp_mask_left : tmp_mask_right;
				shift = shift >> 1;

			}
			if (partially)
			{
				result_p.number[i] = ((extends ? target.number[i] : number[i]) | mask);
			}
			else
			{
				result.number[i] = (number[i] | mask);
			}
			diference = true;
			continue;
		}
		result.number[i] = (this_unchanged ? target.number[i] : number[i]);
		result_p.number[i] = (target_unchanged ? target.number[i] : number[i]);
		extends |= target_unchanged;
		is_extended |= this_unchanged;
		if(extends & is_extended) return false; // if -xx zzz and xxx zz-
	}
	//partially &= this_extends;
	if (partially) result = result_p;
	return true;
}

bool big_number::absorption(const big_number & target/*, big_number & result*/) const
{
	//result.number.resize((n >> 4) + 1);
	//result.n = n;
	bool extends = false, is_extended = false;
	bool this_unchanged, target_unchanged;
	unsigned int and_ = 0;
	if (n != target.n)
	{
		printf("Error(big_number) : different size numbers are compared ");
		exit(1);
	}
	for (int i = n >> 4; i >= 0; i--)
	{
		and_ = number[i] & target.number[i];
		this_unchanged = (and_ == number[i]);
		target_unchanged = (and_ == target.number[i]);
		if (this_unchanged & target_unchanged) // if( true & true)
		{
			//result.number[i] = and;
			continue;
		}
		if (this_unchanged == target_unchanged) // if( false == false)
		{
			return false;
		}
		//result.number[i] = (this_unchanged ? target.number[i] : number[i]);
		extends |= target_unchanged;
		is_extended |= this_unchanged;
		if (extends & is_extended) return false;
	}
	return true;
}

bool big_number::crossing(const big_number& target, big_number& result) const
{
	result.number.resize( (n >> 4) + 1);
	result.n = n;
	//bool is_crossed = false;
	unsigned and_ = 0;
	unsigned mask = 0;
	bool first = true;
	for(int i = n >> 4; i >= 0; i--)
	{
		if (first)
		{
			first = false;
			mask = (0xffffffff >> (32 - ( ((n % 16)+1 )<<1) ));
		}
		else
		{
			mask = 0xffffffff;
		}
		and_ = number[i] & target.number[i];
		unsigned int tmp = ((and_ << 1) | 0x55555555 | and_);
		if( (tmp  & mask) != mask) return false;
		result.number[i] = and_;
	}
	return true;
}

unsigned big_number::get_n() const
{
	return n;
}

bool big_number::is_one() const
{
	if(number[0] == 0) return true;
	if (get_code(n) == 0x1) return false;
	for(auto &i : number)
	{
		if((i>>30) == 0x1) return false;
		if(i)
		{
			if( ((i+1)&i) != 0) return false;
		}
	}
	return true;
}
void big_number::resize(unsigned int n_)
{
	n = n_;
	number.resize( (n + 16)/16 );
}
bool big_number::is_one()
{
	return ((const big_number)*this).is_one();
}

int big_number::get_number_of_code(char code) const
{
	unsigned int count = 0;
	for (unsigned int i = 0; i <= n; i++)
	{
		char cod = (number[i >> 4] >> ((i % 16) << 1)) & 0x3;
		//count += (cod == code) ? 1 : 0;
		if (cod == code)
		{
			count++;
		}
	}
	return count;
}

bool big_number::operator == (const big_number& obj)
{
	/*if (n != obj.n)
	{
		printf("Error(big_number) : different size numbers are compared ");
		exit(1);
	}*/
	for (int i = n >> 4; i >= 0; i--)
	{
		if (number[i] ^ obj.number[i]) return false;
	}
	return true;
}
bool big_number::operator == (const big_number& obj) const
{
	/*if(n != obj.n)
	{
		printf("Error(big_number) : different size numbers are compared ");
		exit(1);
	}*/
	for(int i = n >> 4; i >= 0; i--)
	{
		if(number[i] ^ obj.number[i]) return false;
	}
	return true;
}
bool big_number::operator <  (const big_number& obj)
{
	/*if (n != obj.n)
	{
		printf("Error(big_number) : different size numbers are compared ");
		exit(1);
	}*/
	for (int i = n >> 4; i >= 0; i--)
	{
		if (!(number[i] ^ obj.number[i]))
			continue;
		if (number[i] < obj.number[i])
			return true;
		else return false;
	}
	return false;
}
bool big_number::operator <  (const big_number& obj) const
{
	/*if(n != obj.n)
	{
		printf("Error(big_number) : different size numbers are compared ");
		exit(1);
	}*/
	for(int i = n >> 4; i >= 0; i--)
	{
		if(!(number[i] ^ obj.number[i]) )
			continue;
		if(number[i] < obj.number[i]) 
			return true;
		else return false;
	}
	return false;
}
big_number big_number::operator ^ (const big_number& obj)
{
	big_number res = obj;
	/*if (n != res.n)
	{
		printf("Error(big_number) : different size numbers are compared ");
		exit(1);
	}*/
	for (int i = n >> 4; i >= 0; i--)
	{
		res.number[i] = number[i] ^ obj.number[i];
	}
	return res;
}
big_number big_number::operator ^ (const big_number& obj) const
{
	big_number res = obj;
	/*if(n != res.n)
	{
		printf("Error(big_number) : different size numbers are compared ");
		exit(1);
	}*/
	for(int i = n >> 4; i >= 0; i--)
	{
		res.number[i] = number[i] ^ obj.number[i];
	}
	return res;
}
big_number big_number::operator&(const big_number &obj)
{
	return (const big_number)*this & obj;
}
big_number big_number::operator&(const big_number &obj) const
{
	big_number res = obj;

	if (n != res.n)
	{
		printf("Error(big_number) : different size numbers are compared ");
		exit(1);
	}
	for (int i = n >> 4; i >= 0; i--)
	{
		res.number[i] = number[i] & obj.number[i];
	}
	return res;
}

bool big_number::operator>(const big_number &obj)
{
	if (n != obj.n)
	{
		printf("Error(big_number) : different size numbers are compared ");
		exit(1);
	}
	for (int i = n >> 4; i >= 0; i--)
	{
		if (!(number[i] ^ obj.number[i]))
			continue;
		if (number[i] > obj.number[i])
			return true;
		else return false;
	}
	return false;
}

std::vector<big_number> big_number::get_zhegalkin_vector(unsigned i)
{
	return ((const big_number)*this).get_zhegalkin_vector(i);
}
std::vector<big_number> big_number::get_zhegalkin_vector(unsigned i) const
{
	std::vector<big_number> result_vector;
	result_vector.push_back(*this);
	//for (unsigned i = 0; i < n; i++)
	{
		std::vector<big_number> tmp_vector;
		for (auto &bi : result_vector)
		{
			if (bi.get_code(i) == 0x01)
			{
				big_number tmp_big = bi;
				tmp_big.set_code(i, 0x3);
				tmp_vector.push_back(tmp_big);
				tmp_big.set_code(i, 0x2);
				tmp_vector.push_back(tmp_big);
			}
			else
			{
				tmp_vector.push_back(bi);
			}
		}
		result_vector = tmp_vector;
	}
	//
	return result_vector;
}

std::string big_number::convert2string() const
{
	std::string str;
	bool first = true;
	for (auto &i : number)
	{
		if (!first) str = '_' + str;
		else first = false;
		for (int j = 0; j < 16; j++)
		{
			if (!(j % 8) && j) str = '_' + str;
			char code = (0x3 & (i >> (j << 1)));
			if (code == 0x0) continue;
		    str = ((code == 0x3)?'-': (code == 0x2) ? '1': (code == 0x1)?'0':' ' ) + str;
		}
	}
	return str;
}
