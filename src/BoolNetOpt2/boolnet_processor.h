#pragma once

#include <string>
#include <stdio.h>
#include "boolnet.h"
#include "boolnet_asm.h"
#include "k_function.h"
#include "shannon_path_tree.h"
#include "sf.h"
#include "types.h"
#include "leaf.h"
#include <omp.h>

#ifdef SHORT_ID
#define ID_TYPE unsigned int
#define VOID_ID 0xffffffff
#define ONE_ID  0xfffffffe
#define ZERO_ID 0xfffffffd
#define FUNC2_TYPE1 0x00055a62
#define FUNC2_TYPE2 0x4055a621
#define FUNC2_TYPE3 0x855a6212
#define FUNC2_TYPE4 0xc5a62123
#define FUNC2_TYPE5 0xe5a62123
#define FUNC2_TYPE_MASK 0x1fffffff
#else
#define ID_TYPE unsigned long long
#define VOID_ID 0xffffffffffffffff
#define ONE_ID  0xfffffffffffffffe
#define ZERO_ID 0xfffffffffffffffd
#define FUNC2_TYPE1 0x000055500aa66220
#define FUNC2_TYPE2 0x40055500aa662201
#define FUNC2_TYPE3 0x8055500aa6622012
#define FUNC2_TYPE4 0xc55500aa66220123
#define FUNC2_TYPE5 0xe55500aa66220123
#define FUNC2_TYPE_MASK 0x1fffffffffffffff
#endif

class boolnet_processor
{
protected:
	bool done = false;

	sf          sf0;
	input_param param;

	std::set<ID_TYPE> leaf2use;

	unsigned int min_cnt;
	boolnet*     net;
	shannon_path_tree shpth_tree;

	std::string           res_order;
	std::set<std::string> prev_order_set;
	unsigned int          shannon_path_iter = 0;
	unsigned int          not_so_good_cnt = 0;
	bool                  rand_on = false;// true;

	ID_TYPE  get_id(std::string name);
	boolnet* get_init_bool_net(sf sf0, std::set<leaf*>& unused_leafs);
	boolnet* shannon_expansion(boolnet* net, ID_TYPE id);
	boolnet* get_net_by_specific_order(boolnet* net, std::string order, std::set<leaf*>& unused_leafs, sf sf0);

	boolnet* get_net_from_equals(std::vector<std::string> eq);

public:
	std::vector<std::string> result_equals;

	boolnet_processor(sf sf0, input_param param);
	~boolnet_processor();

	bool get_result();
	bool get_done();
};