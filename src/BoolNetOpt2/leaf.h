#pragma once
#include <stdlib.h>
#include <vector>
#include <string>
#include <set>
#include <map>
#include "big_number.h"
#include "base_node.h"

class leaf : public base_node
{
    protected:
		char op;

		base_node* get_instance_copy(ID_TYPE func_id_);

		void get_cunj(std::set<big_number>& bn_set, const big_number& conj, unsigned int pos, unsigned int left_pos, unsigned int right_pos);
    public:
		static const int LEFT = 0;
		static const int RIGHT = 1;

		//public methods
		leaf(ID_TYPE func_id_, unsigned char status_ = 0);
		leaf(ID_TYPE func_id_, char op_type_, unsigned char status_ = 0);

		void update();

		std::string get_result_function(std::string(*name_by_id) (ID_TYPE id));
		std::string get_inv_equal(std::string(*name_by_id) (ID_TYPE id));
		std::string get_equal(std::string(*name_by_id) (ID_TYPE id));
		bool is_it_equal(base_node*);
		bool is_inv_equal(base_node*);

		ID_TYPE get_id_0();
		ID_TYPE get_id_1();

		base_node* get_left();
		base_node* get_right();
		char  get_op();

		void get_DNF(std::set<big_number> &dnf_set, std::map<base_node*, int> &leaf_map, int color_);

		virtual void set_translate(base_node* to, bool inv = false);
		void set_left(base_node* left_);
		void set_right(base_node* right_);

		ID_TYPE get_key();
};