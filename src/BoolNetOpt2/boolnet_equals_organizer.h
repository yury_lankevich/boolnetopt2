//#pragma once
#include <stdlib.h>
#include <string>
#include <set>
#include "leaf.h"
#include "Tokenizer.h"
class boolnet_equals_organizer
{
protected:
	bool left_complex = false;
	bool right_complex = false;
	std::string sub_eq_left = "", sub_eq_right = "";
	char op_type = '=';
	std::string name = "";
	boolnet_equals_organizer* left_complex_o = nullptr;
	boolnet_equals_organizer* right_complex_o = nullptr;
public:
	boolnet_equals_organizer(std::string, std::string);
	bool is_left_complex();
	bool is_right_complex();
	std::string get_left_eq();
	std::string get_right_eq();
	char get_op_type();
	std::string get_name();
	//void get_simple_set(std::set<boolnet_equals_organizer*> &trg_set);
	void get_simple_set(std::vector<boolnet_equals_organizer*> &trg_set);

	void print();

	bool operator< (const boolnet_equals_organizer& obj) const;

	/*struct comparator
	{
		bool operator()(boolnet_equals_organizer* a, boolnet_equals_organizer* b)
		{
			return (*a) < (*b);
		}
	};
	*/
	static unsigned int tmp_cnt;
};