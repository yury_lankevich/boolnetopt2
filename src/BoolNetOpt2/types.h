#ifndef TYPES_H
#define TYPES_H
struct input_param 
{
	char* output_file;  
	char* input_file;
	char* tagini_file;
	bool targini_isSet;
	bool xor_;
	char* optionini_file;
	int verbosity;
	int type;
	unsigned int max_iter;
	int max_time_minutes;
	char* prj_name;
	char* order;
	bool reduction_only;
};

#endif