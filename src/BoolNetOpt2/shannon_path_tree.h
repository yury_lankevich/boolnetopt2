//#pragma once

#include <string>
#include <vector>
#include <set>
#include <algorithm>
#define SHORT_ID
#ifdef SHORT_ID
#define ID_TYPE unsigned int
#define VOID_ID 0xffffffff
#else
#define ID_TYPE unsigned long long
#define VOID_ID 0xffffffffffffffff
#endif
class shannon_path_tree
{
protected:
	struct shannon_tree_node
	{
		ID_TYPE id = 0;
		unsigned int count = 0;
		shannon_tree_node* parent;
		std::set<shannon_tree_node*> child_nodes;
	};

	unsigned int max_count = 0;

	std::set<shannon_tree_node*> all_leafs;
	shannon_tree_node* latest_parent_node = nullptr;

public:
	std::set<shannon_tree_node*> child_nodes;

	shannon_path_tree();
	~shannon_path_tree();

	void add_item(ID_TYPE id, shannon_tree_node* node = nullptr);
	void add_item(ID_TYPE id, ID_TYPE parent_id);
	void delete_item(shannon_tree_node* node);
	void delete_item(ID_TYPE id);

	void reset_to_root();
	ID_TYPE get_next();

	unsigned int get_size();

	bool is_empty();
};