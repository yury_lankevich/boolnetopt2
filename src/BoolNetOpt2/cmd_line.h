#include "types.h"
#include "Tokenizer.h"
#include <fstream>
#include "tclap\CmdLine.h"
#include "stdlib.h"
#include <string.h>
class cmd_line
{
	protected:
		input_param commandLine(int pcnt,char * pstr[]);
	public:
		input_param param;
		cmd_line(int pcnt,char * pstr[]); //Constructor
		virtual ~cmd_line(void);
};