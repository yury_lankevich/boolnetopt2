#include "base_node.h"

void base_node::fields_init()
{
	level = 0;
	num_parents = 0;
	items_beneath_cnt = 0;
	parents_array_size = 0;
	linked_down_array_size = 0;
}

base_node* base_node::get_instance_copy(ID_TYPE func_id_)
{
	return new base_node(func_id_, status);
}

ID_TYPE base_node::get_next_id()
{
	return tmp_cnt++;
}

base_node::base_node(ID_TYPE func_id_, unsigned char status_)
{
	//all_nodes.insert(this);
	fields_init();
	status = (leaf_type_e)status_;
	func_id = func_id_;
}

base_node::~base_node()
{
	//all_nodes.erase(this);
	if (status & MARKED2DELETE)
	{
		for (unsigned int i = 0; i < num_parents; i++)
		{
			for (unsigned int j = 0; j < linked_up_array[i]->items_beneath_cnt; j++)
			{
				if (linked_up_array[i]->linked_down_array[j] == this)
				{
					linked_up_array[i]->linked_down_array[j] = nullptr;
					break;
				}
			}
		}
		for (unsigned int i = 0; i < items_beneath_cnt; i++)
		{
			if (linked_down_array[i] != nullptr)
			{
				linked_down_array[i]->delete_linked_up(this);
			}
		}
	}
	if (parents_array_size != 0)
	{
		delete[] linked_up_array;
	}
	if (linked_down_array_size != 0)
	{
		delete[] linked_down_array;
		delete[] inv;
	}
}

base_node* base_node::get_copy_leaf(int clr_)
{
	return get_copy_leaf(clr_, clr_, 0);
}

base_node* base_node::get_copy_leaf(int source_clr1_, int target_clr2_, bool change_id)
{
	if (target_clr2_ == color)
	{
		return copy;
	}
	if (source_clr1_ != color && source_clr1_ != target_clr2_)
	{
		return this;
	}
	color = target_clr2_;
	copy = get_instance_copy(change_id ? get_next_id() : func_id);// new leaf(change_id ? get_next_id() : func_id, id_0, id_1, op, status);
	copy->items_beneath_cnt = items_beneath_cnt;
	if (items_beneath_cnt > 0)
	{
		if (copy->linked_down_array_size < items_beneath_cnt)
		{
			if (copy->linked_down_array_size != 0)
			{
				delete[] copy->linked_down_array;
				delete[] inv;
			}
			copy->linked_down_array = new base_node * [items_beneath_cnt];
			copy->linked_down_array_size = items_beneath_cnt;
			copy->inv = new bool[items_beneath_cnt];
		}
		for (unsigned int i = 0; i < items_beneath_cnt; i++)
		{
			copy->linked_down_array[i] = linked_down_array[i]->get_copy_leaf(source_clr1_, target_clr2_, change_id);
			copy->linked_down_array[i]->add_linked_up(copy);
			copy->inv[i] = inv[i];
		}
	}
	if (!change_id)
	{
		copy->func_id = func_id; // TODO: do we need this?
	}
	copy->color = color;
	return copy;
}

void base_node::change_id(int source_clr1_, int target_clr2_)
{
	if (target_clr2_ == color)
	{
		return;
	}
	if (source_clr1_ != color)
	{
		return;
	}
	func_id = get_next_id();
	for (unsigned int i = 0; i < items_beneath_cnt; i++)
	{
		linked_down_array[i]->change_id(source_clr1_, target_clr2_);
	}
	color = target_clr2_;
}

void base_node::set_leaf_type(unsigned char status_)
{
	status = (leaf_type_e)status_;
}

bool base_node::is_output()
{
	return (bool)(status & OUTPUT);
}

void base_node::get_leaf_set(std::vector<base_node*>& trg_set, int color_)
{
	if (color != color_) {
		color = color_;
		trg_set.push_back(this);
		for (unsigned int i = 0; i < items_beneath_cnt; i++)
		{
			linked_down_array[i]->get_leaf_set(trg_set, color);
		}
	}
}

void base_node::get_inputs(std::vector<base_node*>& vector, int src_color_, int trg_color_)
{
	if (color != trg_color_) {
		if (get_leaf_type() & INPUT)
		{
			if (color == src_color_)
			{
				vector.push_back(this);
			}
			color = trg_color_;
			return;
		}
		for (unsigned int i = 0; i < items_beneath_cnt; i++)
		{
			linked_down_array[i]->get_inputs(vector, src_color_, trg_color_);
		}
		color = trg_color_;
	}
}

bool base_node::is_it_equal(base_node*)
{
	return false;
}

bool base_node::is_inv_equal(base_node*)
{
	return false;
}

unsigned char base_node::get_leaf_type()
{
	return status;
}

void base_node::get_DNF(std::set<big_number>& dnf_set, std::map<base_node*, int>& leaf_map, int color_)
{
	printf("[Error: get_DNF] get_DNF() method should not be called from base_node");
}

ID_TYPE base_node::get_func_id()
{
	if (!(get_leaf_type() & OUTPUT))
	{
		if (get_leaf_type() & ZERO) return ZERO_ID;
		if (get_leaf_type() & ONE) return ONE_ID;
	}
	return func_id;
}

int base_node::get_color()
{
	return color;
}

void base_node::add_linked_up(base_node* linked)
{
	if (parents_array_size - num_parents == 0) // Re-allocate memory
	{
		parents_array_size += /*parents_array_size/10 +*/ 8;
		base_node** tmp_array = new base_node * [parents_array_size];
		for (unsigned int i = 0; i < num_parents; i++)
		{
			tmp_array[i] = linked_up_array[i];
		}
		if (num_parents != 0)
		{
			delete[] linked_up_array;
		}
		linked_up_array = tmp_array;
	}
	/*for (unsigned int i = 0; i < num_parents; i++)
	{
		if (linked_up_array[i] == linked)
		{
			printf("\nWe don't need same items!\n");
		}
	}*/
	linked_up_array[num_parents++] = linked;
}

void base_node::delete_linked_up(base_node* linked)
{
	if (num_parents == 0) return;
	for (unsigned int i = 0; i < num_parents; i++)
	{
		if (linked_up_array[i] == linked)
		{
			--num_parents;
			linked_up_array[i] = linked_up_array[num_parents];
			return;
		}
	}
}

void base_node::mark_to_delete()
{
	status = (leaf_type_e)(status | MARKED2DELETE);
}

void base_node::reduce(int color_)
{
	if (color == color_)
	{
		return;
	}
	else
	{
		color = color_;
		if (get_leaf_type() & (INPUT | ZERO | ONE))
			return;
		/* TODO:
		if (op == '*' || op == '+' || op == '@' || op == '%') // @ -> a == b ... % -> a xor b
		{
			if (get_left() == nullptr || get_right() == nullptr)
			{
				printf("Error: leaf:310\n");
			}
		}*/
		lock_update = true;
		for (unsigned int i = 0; i < items_beneath_cnt; i++)
		{
			linked_down_array[i]->reduce(color_);
		}
		lock_update = false;
		update();
		// TODO: ��������� ����������� �� �������
		// ��� op == +, left->child(one of) == ^ right->child(one of)
		//              left->child(another on) == right->child(another one)->child(one of)
		// left->op == *; right->op == *; right->child(another one)->op== +
		// ��������� ��������...
		// �������� ��� ������ ��� ������ �������!!!
		// ��������� ��� ��������� ����� �������� ������������ �� ���� ����������
		/*if (left != nullptr && right != nullptr &&
			left->left != nullptr && left->right != nullptr &&
			right->left != nullptr && right->right != nullptr)
		{
			std::set<big_number> dnf_set;
			//std::map<int, leaf*> leaf_map;
			std::map<leaf*, int> leaf_map;
			this->get_DNF(dnf_set, leaf_map, 0);
			left->get_DNF(dnf_set, leaf_map, 0);
			right->get_DNF(dnf_set, leaf_map, 0);
			{
				std::vector<leaf*> nd2elim_vec;
				for (auto& i : leaf_map)
				{
					for (auto& j : leaf_map)
					{
						if (j != i)
						{
							if (i.first->is_child(j.first)) {
								nd2elim_vec.push_back(i.first);
							}
						}
					}
				}
				for (auto& i : nd2elim_vec)
				{
					i->get_DNF(dnf_set, leaf_map);
				}
			}
		}*/
	}
}

ID_TYPE base_node::get_key()
{
	printf("[Error:get_key()] unexpected usage of get_key() of base_node, should be used method of derived class");
	return 0;
}

bool base_node::check_up_connection()
{
	return num_parents == 0;
}

void base_node::change_color(int color_)
{
	if (color != color_)
	{
		color = color_;
		for (unsigned int i = 0; i < items_beneath_cnt; i++)
		{
			linked_down_array[i]->change_color(color);
		}
	}
}

void base_node::change_color_for_all_parents(int color_)
{
	if (color != color_)
	{
		color = color_;
		for (unsigned int i = 0; i < num_parents; i++)
		{
			linked_up_array[i]->change_color_for_all_parents(color_);
		}
	}
}

void base_node::replace(base_node* to, bool inv_)
{
	for (unsigned int i = 0; i < items_beneath_cnt; i++)
	{
		linked_down_array[i]->delete_linked_up(this);
	}
	for (unsigned int i = 0; i < num_parents; i++)
	{
		leaf_type_e prev_type = (leaf_type_e)linked_up_array[i]->get_leaf_type();
		linked_up_array[i]->replace_one_by_another(this, to, inv_);
		if (!(linked_up_array[i]->get_leaf_type() & EXCLUDED))
		{
			linked_up_array[i]->update();
		}
	}
	set_leaf_type(EXCLUDED);
	copy = to;
	num_parents = 0;
	parents_array_size = 0;
	items_beneath_cnt = 0;
	inv[0] = inv_;
	//delete[] linked_down_array;
	//delete[] inv;
	delete[] linked_up_array;
}

void base_node::replace_one_by_another(base_node* from, base_node* to, bool inv_)
{
	bool do_not_add_linked_up = false;
	for (unsigned int i = 0; i < items_beneath_cnt; i++)
	{
		if (to == linked_down_array[i])
		{
			do_not_add_linked_up = true;
		}
		if (linked_down_array[i] == from)
		{
			linked_down_array[i] = to;
			inv[i] = inv_ ^ inv[i];
		}
	}
	if (!do_not_add_linked_up)
	{
		to->add_linked_up(this);
	}
}

void base_node::update()
{
	printf("[ERROR][base_node]: update should not be used, it should be overrided");
}

unsigned base_node::update_level(int color_)
{
	if (color == color_) return level;
	color = color_;
	if (get_leaf_type() & (INPUT | ONE | ZERO | EXCLUDED))
	{
		return level = 0;
	}
	else
	{
		unsigned max_level = 0;
		for (unsigned int i = 0; i < items_beneath_cnt; i++)
		{
			level = linked_down_array[i]->update_level(color);
			if (max_level < level)
				max_level = level;
		}
		level = max_level;
	}
	return ++level;
}

void base_node::set_translate(base_node* to, bool inv_)
{
	for (unsigned int i = 0; i < items_beneath_cnt; i++)
	{
		linked_down_array[i]->delete_linked_up(this);
	}
	items_beneath_cnt = 1;
	if (linked_down_array_size < 1)
	{
		linked_down_array_size = 1;
		linked_down_array = new base_node * [1];
		inv = new bool[1];
	}
	inv[0] = inv_;
	linked_down_array[0] = to;
	linked_down_array[0]->add_linked_up(this);
	status = (leaf_type_e)((status & (OUTPUT)) | TRANSLATED);
}
