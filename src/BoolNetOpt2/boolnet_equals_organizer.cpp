#include "boolnet_equals_organizer.h"

boolnet_equals_organizer::boolnet_equals_organizer(std::string eq, std::string name_)
{
	name = name_;
	std::vector<std::string> split_string;
	//TODO
	CTokenizer<CIsChar<' '>>::Tokenize(split_string, eq);
	if ( (split_string.size() >= 2) || (split_string[0].length() != eq.length()))
	{
		printf("Error: space in eq");
		//for (auto &item : split_string)
		//{
		//	
		//}
	}
	CTokenizer<CIsChar<'+'>>::Tokenize(split_string, eq);
	if (split_string.size() >= 2)
	{
		op_type = '+';
		if (split_string.size() > 2)
		{
			int i = 0;
			sub_eq_left = split_string[i++];
			left_complex = true;
			for (; i <= (split_string.size() - 1) / 2; i++)
			{
				sub_eq_left += "+" + split_string[i];
			}
			sub_eq_right = split_string[i++];
			for (; i < split_string.size() ; i++)
			{
				right_complex = true;
				sub_eq_right += "+" + split_string[i];
			}
		}
		else
		{
			sub_eq_left = split_string[0];
			sub_eq_right = split_string[1];
			if (sub_eq_left.find('*') != std::string::npos)
			{
				left_complex = true;
			}
			else if(sub_eq_left.find('^') != std::string::npos)
			{
				left_complex = true;
			}
		}
		if (!right_complex)
		{
			if (sub_eq_right.find('*') != std::string::npos)
			{
				right_complex = true;
			}
			else if (sub_eq_right.find('^') != std::string::npos)
			{
				right_complex = true;
			}
		}
	}
	else
	{
		CTokenizer<CIsChar<'*'>>::Tokenize(split_string, eq);
		if (split_string.size() >= 2)
		{
			op_type = '*';
			if (split_string.size() > 2)
			{
				int i = 0;
				sub_eq_left = split_string[i++];
				left_complex = true;
				for (; i <= (split_string.size() - 1) / 2; i++)
				{
					sub_eq_left += "*" + split_string[i];
				}
				sub_eq_right = split_string[i++];
				for (; i < split_string.size(); i++)
				{
					right_complex = true;
					sub_eq_right += "*" + split_string[i];
				}
			}
			else
			{
				sub_eq_left = split_string[0];
				sub_eq_right = split_string[1];
				if (sub_eq_left.find('^') != std::string::npos)
				{
					left_complex = true;
				}
			}
			if (!right_complex)
			{
				if (sub_eq_right.find('^') != std::string::npos)
				{
					right_complex = true;
				}
			}
		}
		else
		{
			if (eq[0] == '^')
			{
				op_type = '^';
				CTokenizer<CIsChar<'^'>>::Tokenize(split_string, eq); //TODO: easier !
				sub_eq_left = split_string[0];
			}
			else
			{
				op_type = '=';
				sub_eq_left = eq;
			}

		}
	}



	/*std::string res_eq ;*/
	if (left_complex)
	{
		std::string left_name = "TMP_" + std::to_string(tmp_cnt++);
		left_complex_o = new boolnet_equals_organizer(sub_eq_left, left_name);
		sub_eq_left = left_name;
		/*if (op_type = '^')
		{
			res_eq = op_type + sub_eq_left;
		}
		else
		{
			res_eq = sub_eq_left;
		}*/
	}
	else
	{
		if (sub_eq_left.find('*') != std::string::npos) {
			left_complex = true;
		}
	}
	if (right_complex)
	{
		std::string right_name = "TMP_" + std::to_string(tmp_cnt++);
		right_complex_o = new boolnet_equals_organizer(sub_eq_right, right_name);
		sub_eq_right = right_name;
		/*res_eq = res_eq + op_type + right_name;*/
	}
	else
	{
		if (sub_eq_right.find('*') != std::string::npos) {
			right_complex = true;
		}
	}

	/*printf("%s = %s\n", name.data() , res_eq.data());*/
}


bool boolnet_equals_organizer::is_left_complex()
{
	return left_complex;
}

bool boolnet_equals_organizer::is_right_complex()
{
	return right_complex;
}

std::string boolnet_equals_organizer::get_left_eq()
{
	return sub_eq_left;
}

std::string boolnet_equals_organizer::get_right_eq()
{
	return sub_eq_right;
}

char boolnet_equals_organizer::get_op_type()
{
	return op_type;
}

std::string boolnet_equals_organizer::get_name()
{
	return name;
}

void boolnet_equals_organizer::get_simple_set(std::vector<boolnet_equals_organizer*> &trg_set)
{
	if (left_complex_o != nullptr)
	{
		trg_set.push_back(left_complex_o);
		//trg_set.insert(left_complex_o);
		left_complex_o->get_simple_set(trg_set);
	}
	if (right_complex_o != nullptr)
	{
		trg_set.push_back(right_complex_o);
		//trg_set.insert(right_complex_o);
		right_complex_o->get_simple_set(trg_set);
	}
}

void boolnet_equals_organizer::print()
{
	if (op_type == '=')
	{
		printf("%s=%s;\n", name.data(), sub_eq_left.data());
	}
	else if (op_type != '^')
	{
		printf("%s=%s%c%s;\n", name.data(), sub_eq_left.data(), op_type, sub_eq_right.data());
	}
	else
	{
		printf("%s=^%s;\n", name.data(), sub_eq_left.data());
	}
}

bool boolnet_equals_organizer::operator<(const boolnet_equals_organizer& obj) const
{
	return this->name < obj.name;
}
