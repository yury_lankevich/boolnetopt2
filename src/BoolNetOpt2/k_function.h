#pragma once
#include <stdlib.h>
#include <set>
#include <string>
#include <algorithm>
#include "big_number.h"
#include <chrono>
#include <ctime>

class k_function
{
    protected:
		bool zhegalkin;
		std::set<big_number> conjunctions;
		std::vector<big_number> zhegalkin_polinom;
		std::set<int> essential_vars;
    public: 
		static unsigned char type;
		k_function(std::vector<std::string>& m_in, bool it_is_zh = false, std::string fname = nullptr);
		k_function();

		void exclude_pos(std::vector<unsigned int>& positions);
		void exclude_pos(std::vector<unsigned int>& positions, std::vector<char> codes);

		bool is_one();
		void add_conjunction(big_number&);
		void add_conjunction(std::set<big_number>&);
		virtual void minimization();
		bool there_is_inputs(unsigned pos, char val = 3); // 0x01 means 0, 0x02 means 1, 0x3 - any
		void glew_in_the_layer(const big_number &i, big_number &tmp_big, std::set<big_number> &next_layer, std::set<big_number> &ll, bool &is_changed, bool &already_is, bool glew_on_err = false);

		size_t size();

		big_number           get_descriptor();

		void to_zhegalkin_polinom(std::string fname);
		unsigned int get_complexity();

		bool operator == (const k_function&) ;
		bool operator == (const k_function&) const;
		bool operator <  (const k_function&) ;
		bool operator <  (const k_function&) const;
};