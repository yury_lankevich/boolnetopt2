#include "boolnet.h"

boolnet::boolnet()
{
}

boolnet::~boolnet()
{
	for (auto &i : all_system_nodes)
	{
		delete i;
	}
	all_system_nodes.clear();
}

void boolnet::add_system_root(base_node* system_root)
{
	system_roots.push_back(system_root);// .insert(system_root);
	all_system_nodes.push_back(system_root);
}

void boolnet::add_system_leaf(base_node* system_leaf)
{
	system_leafs.insert(system_leaf);
	all_system_nodes.push_back(system_leaf);
}

void boolnet::add_internal_node(base_node* node)
{
	all_system_nodes.push_back(node);
}

void boolnet::reduce(int mode)
{
	int color = get_next_color();
	std::vector<base_node*> all_nodes;

	//printf("Reducing first stage started...\n");
	for (auto &i : system_roots)
	{
		i->reduce(color);
	}
	system_leafs.clear();
	all_nodes = all_system_nodes;
	all_system_nodes.clear();
	for (auto &i : all_nodes)
	{
		if (i->is_output())
		{
			add_internal_node(i);
		}
		else
		{
			if (i->get_leaf_type() & INPUT)
			{
				add_system_leaf(i);
			}
			else if (i->get_leaf_type() & (ONE | ZERO))
			{
				i->mark_to_delete();
				delete i;
			}
			else if (i->get_leaf_type() & EXCLUDED)
			{
				i->mark_to_delete();
				delete i;
			}
			else
			{
				add_internal_node(i);
			}
		}
	}
	clean_up();

	//printf("Reducing second stage started...\n");
	//printf("========= We start additional replacment here =================\n");
	if (mode == 1) // TODO make via tree ? from leafs to roots
	{
		additional_reduction();
	}
}

unsigned int boolnet::get_nodes_cnt()
{
	size_t cnt = all_system_nodes.size();
	for (auto &i : all_system_nodes)
	{
		if (i->get_leaf_type() & (INPUT | EXCLUDED | ZERO | ONE | TRANSLATED))
		{
			cnt--;
		}
		else if (((leaf*)i)->get_op() == '=')
		{
			cnt--;
		}
		else if (type == 4)
		{
			if (((leaf*)i)->get_op() == '%')
			{
				cnt += 1;
			}
			else if (((leaf*)i)->get_op() == '@')
			{
				cnt += 1;
			}
		}
	}
	if (type == 2 || type == 3)
	{
		unsigned int tmp_cnt = 0;
		for (auto &i : merge_result)
		{
			for (auto &j : i)
			{
				if (j == '*' || j == '+')
				{
					tmp_cnt++;
				}
			}
		}
		if (type == 2)
		{
			return cnt +tmp_cnt;
		}
		else // type = 3
		{
			return cnt + tmp_cnt / 10;
		}
	}
	else // type = 1 || type == 4
	{
		return cnt;
	}
}

std::set<std::string> boolnet::get_merge_result()
{
	return merge_result;
}

std::set<std::string> boolnet::get_all_roots()
{
	std::set<std::string> all_roots;
	for (auto &i : system_roots)
	{
		all_roots.insert(((leaf*)i)->get_result_function(get_name_by_id));
	}
	return all_roots;
}

std::vector<std::string> boolnet::convert2string(bool c)
{
	std::vector<std::string> result;
	for (auto &i : all_system_nodes)
	{
		if ( !(i->get_leaf_type() & (ZERO | ONE | EXCLUDED | /*TRANSLATED |*/ INPUT)) || i->is_output() )
		{
			if (c)
			{
				result.push_back(((leaf*)i)->get_result_function(get_name_by_id) + ';');
			}
			else
			{
				result.push_back(((leaf*)i)->get_result_function(get_name_by_id));
			}
		}
	}
	return result;
}

int boolnet::get_next_color()
{
	return ++next_color;
}

boolnet * boolnet::get_copy()
{
	boolnet* copy_net = new boolnet();
	if (system_roots.size() == 0) return copy_net;
	int color = get_next_color();
	for (auto &i : system_leafs)
	{
		base_node* copy = i->get_copy_leaf(color);
		copy_net->add_system_leaf(copy);
	}
	for (auto &i : system_roots)
	{
		copy_net->add_system_root(i->get_copy_leaf(color));
	}
	{
		color = copy_net->get_next_color();
		std::vector<base_node*> all_nodes;
		all_nodes.reserve(all_system_nodes.size());
		for (auto &i : copy_net->system_roots)
		{
			i->get_leaf_set(all_nodes, color);
		}
		copy_net->all_system_nodes = all_nodes;
		copy_net->all_system_nodes.reserve(all_system_nodes.size() * 2);
	}
	return copy_net;
}

void boolnet::shannon_expansion(ID_TYPE lf_id)
{
	int trg_color;
	int src_color;
	std::vector<base_node*> new_roots_set;
	new_roots_set.reserve(system_roots.size() * 2);

	std::map<ID_TYPE, base_node*> one_roots;
	std::map<ID_TYPE, base_node*> zero_roots;
	{
		base_node* one_leaf{};

		//1. find leaf_id
		//2. Paint changed subtree
		src_color = get_next_color();
		for (auto &i : system_leafs)
		{
			if (i->get_func_id() == lf_id)
			{
				i->change_color_for_all_parents(src_color);
				one_leaf = i;
				break; // we have only one input?
			}
		}
		//3. Change ids for first subtree
		trg_color = get_next_color();
		//std::vector<leaf*> one_roots;
		// TODO: we have to avoid this critical section
#pragma omp critical
		{
			for (auto& i : system_roots)
			{
				if (i->get_color() == src_color)
				{
					ID_TYPE cur_id = i->get_func_id();
					i->change_id(src_color, trg_color);
					//one_roots.push_back(i);
					one_roots.insert({ cur_id , i });
				}
				else
				{
					new_roots_set.push_back(i);// .insert(i); // Keep unchanged
				}
			}
		}
		if (one_roots.size() == 0)
		{
			printf("Shannon expansion: warning, inessential variable\n");
			return;
		}
		//4. Copy the subtree and change ids, save zero roots and one roots
		src_color = trg_color;
		trg_color = get_next_color();
		// TODO: we have to avoid this critical section
#pragma omp critical
		{
			for (auto& i : one_roots)
			{
				base_node* curr_root = i.second->get_copy_leaf(src_color, trg_color, true);
				zero_roots.insert({ i.first, curr_root });
				//add_system_root(curr_root);
			}
		}
		//5. Set zero type and one type for leaf copies
		std::vector<base_node*> zero_vec;
		src_color = trg_color;
		trg_color = get_next_color();
		zero_roots.begin()->second->get_inputs(zero_vec, src_color, trg_color);
		if (zero_vec.size() != 1)
		{
			printf("Error: Shannon expansion, wrong vector size\n");
			exit(1);
		}
		zero_vec.front()->set_leaf_type(ZERO);
		one_leaf->set_leaf_type(ONE);

		trg_color = get_next_color();
		system_roots.reserve(system_roots.size() + zero_roots.size());
		for (auto &i : system_roots) // without zero subnet
		{
			i->change_color(trg_color);
		}
		for (auto &i : zero_roots)
		{
			i.second->get_leaf_set(all_system_nodes, trg_color);
			system_roots.push_back(i.second);
			//add_system_root(i.second);
		}
	}
	//6. reduce net

	trg_color = get_next_color();
	for (auto &i : system_roots)
	{
		i->change_color(trg_color);
	}
	std::vector<base_node*> all_nodes;
	all_nodes.reserve(all_system_nodes.size());
	for (auto &i : all_system_nodes)
	{
		if (i->get_color() != trg_color) //(i->is_reduced() == false)
		{
			i->mark_to_delete();
			delete i;
		}
		else
		{
			all_nodes.push_back(i);
		}
	}
	all_system_nodes = all_nodes;

	//printf("Reducing started...\n");
	reduce();
	//printf("Reducing finished...\n");

	//7. Add merge result
	std::string sub_eq1, sub_eq2;
	unsigned char code = 0;
	base_node* first_leaf = nullptr;
	base_node* second_leaf = nullptr;
	ID_TYPE initial_id;
	for (auto &item : one_roots)
	{
		bool skip_one = false;
		code = 0;
		initial_id = item.first;
		first_leaf = item.second;
		second_leaf = zero_roots[initial_id];

		std::string i_name = get_name_by_id(first_leaf->get_func_id());
		std::string s_name;

		if (first_leaf->get_leaf_type() & ZERO)
		{
			code |= 0x02;
			sub_eq1 = "";
		}
		else if (first_leaf->get_leaf_type() & ONE)
		{
			code |= 0x03;
			sub_eq1 = get_name_by_id(lf_id);
		}
		else
		{
			sub_eq1 = get_name_by_id(lf_id) + '*' + i_name;
		}

		if (second_leaf->get_leaf_type() & ZERO)
		{
			code |= 0x20;
			sub_eq2 = "";
			//s_name = "0";
		}
		else if (second_leaf->get_leaf_type() & ONE)
		{
			code |= 0x30;
			sub_eq2 = '^' + get_name_by_id(lf_id);
			//s_name = "1";
		}
		else
		{
			s_name = get_name_by_id(second_leaf->get_func_id());
			sub_eq2 = '^' + get_name_by_id(lf_id) + '*' + s_name;
		}
		switch (code)
		{
		case 0x22:
			merge_result.insert(get_name_by_id(initial_id) + "=0");
			break;
		case 0x33:
			merge_result.insert(get_name_by_id(initial_id) + "=1");
			break;
		case 0x02:
		case 0x32:
			merge_result.insert(get_name_by_id(initial_id) + '=' + sub_eq2);
			break;
		case 0x20:
		case 0x23:
			merge_result.insert(get_name_by_id(initial_id) + '=' + sub_eq1);
			break;
		default:
			std::string eq1, eq2;
			leaf* tmp_leaf;
			if (first_leaf->get_leaf_type() & TRANSLATED)
			{
				if (((leaf*)first_leaf)->get_left()->get_leaf_type() & INPUT)
				{
					eq1 = ((leaf*)first_leaf)->get_equal(get_name_by_id);
				}
				else
				{
					tmp_leaf = dynamic_cast<leaf*> (((leaf*)first_leaf)->get_left());
					if (tmp_leaf != nullptr)
					{
						if (first_leaf->inv[leaf::LEFT])
						{
							eq1 = tmp_leaf->get_inv_equal(get_name_by_id);
						}
						else
						{
							eq1 = tmp_leaf->get_equal(get_name_by_id);
						}
					}
				}
			}
			else
			{
				eq1 = ((leaf*)first_leaf)->get_equal(get_name_by_id);
			}
			if (second_leaf->get_leaf_type() & TRANSLATED)
			{
				if (((leaf*)second_leaf)->get_left()->get_leaf_type() & INPUT)
				{
					eq2 = ((leaf*)second_leaf)->get_equal(get_name_by_id);
				}
				else
				{
					tmp_leaf = dynamic_cast<leaf*> (((leaf*)second_leaf)->get_left());
					if (tmp_leaf != nullptr)
					{
						if (second_leaf->inv[leaf::LEFT])
						{
							eq2 = tmp_leaf->get_inv_equal(get_name_by_id);
						}
						else
						{
							eq2 = tmp_leaf->get_equal(get_name_by_id);
						}
					}
				}
			}
			else
			{
				eq2 = ((leaf*)second_leaf)->get_equal(get_name_by_id);
			}
			if (eq1 == eq2 && eq1 != "")
			{
				skip_one = true;
				merge_result.insert(get_name_by_id(initial_id) + '=' + i_name);
			}
			else
			{
				if (code == 0x30)
				{
					merge_result.insert(get_name_by_id(initial_id) + '=' + i_name + '+' + sub_eq2);
				}
				else if (code == 0x03)
				{
					merge_result.insert(get_name_by_id(initial_id) + '=' + sub_eq1 + '+' + s_name);
				}
				else
				{
					merge_result.insert(get_name_by_id(initial_id) + '=' + sub_eq1 + '+' + sub_eq2);
				}
			}
			break;
		}
		if ((code & 0x03) == 0)
		{
			new_roots_set.push_back(first_leaf);// .insert(first_leaf);
		}
		if ((code & 0x30) == 0 && !skip_one)
		{
			new_roots_set.push_back(second_leaf);// .insert(second_leaf);
		}
	}
	system_roots = new_roots_set;

	trg_color = get_next_color();
	for (auto &i : system_roots)
	{
		i->change_color(trg_color);
	}
	
	all_nodes.clear();
	for (auto &i : all_system_nodes)
	{
		if (i->get_color() != trg_color) //(i->is_reduced() == false)
		{
			i->mark_to_delete();
			if (i->get_leaf_type() & INPUT)
			{
				auto ptr = system_leafs.find(i);
				if (ptr != system_leafs.end())
				{
					system_leafs.erase(ptr);
				}
			}
			delete i;
		}
		else
		{
			all_nodes.push_back(i);
		}
	}
	all_system_nodes = all_nodes;
}

void boolnet::clean_up()
{
	std::vector<base_node*> all_nodes;
	int trg_color = get_next_color();
	for (auto &i : system_roots)
	{
		i->change_color(trg_color);
	}

	all_nodes.clear();
	for (auto &i : all_system_nodes)
	{
		if (i->get_color() != trg_color && !(i->get_leaf_type() & INPUT)) //(i->is_reduced() == false)
		{
			i->mark_to_delete();
			delete i;
		}
		else
		{
			all_nodes.push_back(i);
		}
	}
	all_system_nodes = all_nodes;
}

std::string boolnet::get_name_by_id(ID_TYPE id)
{
	auto ptr = ids_map->find(id);
	std::string name;
	if (ptr == ids_map->end())
	{
		return "TN_" + std::to_string(id);
	}
	else
	{
		return ptr->second;
	}
}

ID_TYPE boolnet::get_id(std::string name)
{
	auto ptr = names_map->find(name);
	ID_TYPE id;
	if (ptr == names_map->end())
	{
		printf("Error boolnet: 732");
		return 0;
	}
	else
	{
		id = ptr->second;
		return id;
	}
}
void boolnet::print(std::string name)
{
	/*FILE* f = fopen(name.data(), "a+");
	for (auto &i : all_system_nodes)
	{
		fprintf(f, "%8s=%17s    $$$ TYPE=%2d PREV_NAME=%s\n", i->get_func_name().data(), i->get_equal().data(), i->get_leaf_type(), i->get_prev_func_name().data());
	}
	fclose(f);*/
	printf("Error: boolnet print is not implemented");
}

void boolnet::clean_merge_result()
{
	merge_result.clear();
}

void boolnet::make_DNF_nodes()
{
	int color = get_next_color();
	int color2 = get_next_color();
	// I. Start from roots
	for (auto &i : system_roots)
	{
		// 1. Painting nodes from roots
		i->change_color(color);
		for (auto& j : system_roots)
		{
			if (j != i)
			{
				j->change_color(color2);
			}
		}
		// 2. Get DNF from each root by color
		std::set<big_number> DNF;
		std::map<base_node*, int> MAP;
		i->get_DNF(DNF, MAP, color);
		DNF_node* n_node = new DNF_node(base_node::get_next_id(), INTERNAL);
		n_node->set_DNF(DNF, MAP);

		// 3. Replace nodes with right color by DNF node / keep only roots
		i->set_translate(n_node);
		// TODO
		// reduce DNF node
		
	}
	// II. Find next level nodes to convert them to DNF nodes
	// TODO:

	// Delete all unused nodes
	clean_up();
}

bool boolnet::replace_if_equal(base_node*& k, base_node*& j)
{
	if (k->is_it_equal(j))
	{
		if (j->is_output())
		{
			if (k->is_output())
			{
				//TODO create new node and add it to boolnet
			}
			else
			{
				j->set_translate(k);
			}
		}
		else if (k->is_output())
		{
			k->set_translate(j);
			return true;
		}
		else
		{
			k->replace(j);
			//printf("Replace %lld to %lld \n", (*k)->get_func_id(), (*j)->get_func_id());
		}
	}
	else if (k->is_inv_equal(j))
	{

		//printf("found inversion\n");
		if (j->is_output())
		{
			if (k->is_output())
			{
				//TODO create new node and add it to boolnet
			}
			else
			{
				j->set_translate(k, true); //TODO
			}
		}
		else if (k->is_output())
		{
			k->set_translate(j, true);
			return true;
		}
		else
		{
			k->replace(j, true);
			//printf("Replace %lld to %lld \n", (*k)->get_func_id(), (*j)->get_func_id());
		}
	}
	return false;
}

void boolnet::additional_reduction()
{
	unsigned max_level = 0;
	int color = get_next_color();//++;
	for (auto& i : system_roots)
	{
		if (max_level < i->update_level(color))
		{
			max_level = i->update_level(color);
		}
	}
	std::vector<std::map<ID_TYPE, group_of_nodes>> groups(max_level);
	{
		std::vector<std::vector<base_node*> > level_set(max_level);
		std::vector<std::vector<base_node*> > level_DNF_set(max_level);
		for (auto& i : all_system_nodes)
		{
			if (i->level != 0 && !(i->is_output() && (i->get_leaf_type() & TRANSLATED)))
			{
				if (dynamic_cast<leaf*>(i) != nullptr)
				{
					level_set[i->level - 1].push_back(i);
				}
				else if(dynamic_cast<DNF_node*>(i) != nullptr)
				{
					level_DNF_set[i->level - 1].push_back(i);
				}
			}
		}
		for (unsigned lvl = 1; lvl <= max_level; lvl++)
		{
			// TODO: �� ���� ������ ������� �� � ����!!!
			// Group by left@right names
			for (auto& i : level_set[lvl - 1])
			{
				ID_TYPE key;
				key = i->get_key();
				auto gr_ptr = groups[lvl - 1].find(key);
				if (gr_ptr == groups[lvl - 1].end())
				{
					group_of_nodes group;
					group.key = key;
					group.group.push_back(i);
					groups[lvl - 1].insert({ key, group });
				}
				else
				{
					gr_ptr->second.group.push_back(i);
				}
			}
			for (auto& i : groups[lvl - 1])
			{
				if (i.second.group.size() > 1)
				{
					//printf("group size : %0d ... level : %0d \n", i.second.group.size(), (*i.second.group.begin())->level);
					for (auto j = i.second.group.begin(); j != i.second.group.end() - 1; j++)
					{
						if (!((*j)->get_leaf_type() & (EXCLUDED | ONE | ZERO | TRANSLATED)))
						{
							if (lvl == (*j)->level) {
								for (auto k = j + 1; k != i.second.group.end(); k++)
								{
									if (!((*k)->get_leaf_type() & (EXCLUDED | ONE | ZERO | TRANSLATED)))
									{
										if (replace_if_equal(*k, *j)) break;
									}
								}
							}
						}
					}
					//TODO compare + inv
				}
			}
		}
	}

	while (true)
	{
		bool need2b_reduced_again = false;
		color = get_next_color();//++;
		for (auto& i : system_roots) // TODO: check why level doesn't calculate properly in loop above... maybe add some flag to track such case?
		{
			if (max_level < i->update_level(color))
			{
				max_level = i->update_level(color);
			}
		}

		std::vector<std::map<ID_TYPE, group_of_nodes>> tmp_gr(max_level);
		for (unsigned lvl = 1; lvl <= max_level; lvl++)
		{
			for (auto& i : groups[lvl - 1])
			{
				std::vector<base_node*> new_group;
				for (auto& j : i.second.group)
				{
					if (!(j->get_leaf_type() & (EXCLUDED|ONE|ZERO)) && !(j->is_output() && (j->get_leaf_type() & TRANSLATED)))
					{
						ID_TYPE key = j->get_key();
						if (j->level != lvl || i.second.key != key)
						{
							auto gr_ptr = tmp_gr[j->level - 1].find(key);
							if (gr_ptr == tmp_gr[j->level - 1].end())
							{
								group_of_nodes group;
								group.key = key;
								group.group.push_back(j);
								tmp_gr[j->level - 1].insert({ key, group });
							}
							else
							{
								gr_ptr->second.group.push_back(j);
							}
							need2b_reduced_again = true;
						}
						else
						{
							new_group.push_back(j);
						}
					}
				}
				i.second.group = new_group;
			}
		}
		if (!need2b_reduced_again) break;
		for (unsigned lvl = 1; lvl <= max_level; lvl++)
		{
			for (auto& i : tmp_gr[lvl - 1])
			{
				if (i.second.group.size() > 0)
				{
					auto gr_ptr = groups[lvl - 1].find(i.second.key);
					//printf("group size : %0d ... level : %0d \n", i.second.group.size(), (*i.second.group.begin())->level);
					for (auto j = i.second.group.begin(); j != i.second.group.end(); j++)
					{
						if (!((*j)->get_leaf_type() & (EXCLUDED | ONE | ZERO | TRANSLATED)))
						{

							//if (lvl != (*j)->level)//update_level()) // Update lvlv
							//{
							//	//need2b_reduced_again = true;
							//	//printf("level is different (%lld != %lld)\n", lvl, (*j)->level);
							//}
							//else
							if (lvl == (*j)->level)
							{
								for (auto k = j + 1; k != i.second.group.end(); k++)
								{
									if (!((*k)->get_leaf_type() & (EXCLUDED | ONE | ZERO | TRANSLATED)))
									{
										if (replace_if_equal(*k, *j)) break;
									}
								}
								if (gr_ptr != groups[lvl - 1].end())
								{
									for (auto &k : gr_ptr->second.group)
									{
										if (!(k->get_leaf_type() & (EXCLUDED | ONE | ZERO | TRANSLATED)))
										{
											if (replace_if_equal(k, *j)) break;
										}
									}
								}
							}
						}
					}
					if (gr_ptr == groups[lvl - 1].end())
					{
						group_of_nodes group;
						group.key = i.second.key;
						groups[lvl - 1].insert({ i.second.key, group });
					}
				}
			}
			groups[lvl - 1].insert(tmp_gr[lvl - 1].begin(), tmp_gr[lvl - 1].end());
		}
		/*

		 1. ����� ����, � ���� �� ��������� level -> �������� � ������ ������ �� �������
		 2. ������� �� ������ ������� ������ (� ������� �� ��������� level)
		 3. ��� ���� ���� ����� � ������ level'�� ��������� �� ������ � ������ �� ���� �� ������ � �� ����� ������ (��� ������� ����� �� �������)
		 4. ��������� �� ��������� �� � ���� ����� �����
		for (unsigned lvl = 1; lvl <= max_level; lvl++)
		{
			for (auto& i : level_set[lvl - 1])
			{
				if (i->level != 0 && !(i->is_output() && (i->get_leaf_type() & TRANSLATED)))
				{
					if (i->level != lvl)
					{
						need2b_reduced_again = true;
						// TODO: add to group to check with prev group
						break; // then remove break
					}
				}
			}
		}*/
	}
}
