#include "boolnet_asm.h"
boolnet_asm::boolnet_asm()
{
	blnet_o = new boolnet();
}

boolnet_asm::boolnet_asm(std::vector<std::string> &equals, std::vector<std::string> &in_names_, std::vector<std::string> &out_names_)
{
	names_map.insert( (boolnet::names_map)->begin(), (boolnet::names_map)->end());
	names_map.insert({ "1" , ONE_ID });
	names_map.insert({ "0" , ZERO_ID });
	out_names.insert(out_names_.begin(), out_names_.end());
	in_names.insert(in_names_.begin(), in_names_.end());
	blnet_o = new boolnet();
	for (auto &i : equals) {
		add_equal(i);
	}
	generate_boolnet_by_simple_func_set();
}

void boolnet_asm::add_equal(std::string eq)
{
	std::vector<std::string> oResult;
	CTokenizer<CIsChar<'='>>::Tokenize(oResult, eq);
	if (oResult.size() > 2) {
		printf("Error: Too much chracters '=' in equal");
	}
	else if (oResult.size() < 2) {
		printf("Error: there is no chracter '=' in equal");
	}

	boolnet_equals_organizer *eq_organizer_h = new boolnet_equals_organizer(oResult[1], oResult[0]);
	simple_func_set.push_back(eq_organizer_h);
	eq_organizer_h->get_simple_set(simple_func_set);
}

void boolnet_asm::generate_boolnet_by_simple_func_set()
{
	std::vector<node_and_eq> all_system_nodes;
	std::map<ID_TYPE, leaf*> leaf_ids_map;
	// Add all functions
	for (auto &i : simple_func_set)
	{
		node_and_eq node_eq_item;
		leaf* current_leaf;
		auto find_res_outp = std::find(out_names.begin(), out_names.end(), i->get_name());
		ID_TYPE id = get_id(i->get_name());
		if (find_res_outp != out_names.end()) // Check wether it is OUTPUT
		{
			current_leaf = new leaf(id, i->get_op_type(), OUTPUT);
			blnet_o->add_system_root(current_leaf);
		}
		else
		{
			current_leaf = new leaf(id, i->get_op_type());
			blnet_o->add_internal_node(current_leaf);
		}
		leaf_ids_map.insert({ id, current_leaf });
		node_eq_item.eq = i;
		node_eq_item.node = current_leaf;
		all_system_nodes.push_back(node_eq_item);
	}
	// Add INPUT leaf
	std::set<leaf*> all_inputs;
	for (auto &i : in_names)
	{
		leaf* current_leaf;
		current_leaf = new leaf(get_id(i), (unsigned char)INPUT);
		all_inputs.insert(current_leaf);
		leaf_ids_map.insert({ get_id(i), current_leaf });
	}
	std::set<leaf*> const2delete;
	{
		auto i = new leaf(ONE_ID, (unsigned char)ONE);
		const2delete.insert(i);
		leaf_ids_map.insert({ ONE_ID, i });
		i = new leaf(ZERO_ID, (unsigned char)ZERO);
		const2delete.insert(i);
		leaf_ids_map.insert({ ZERO_ID, i });
	}

	// Connect all nodes
	//printf("Connect all nodes\n");
	for (auto &i : all_system_nodes)
	{
		if (i.node->get_leaf_type() & ( ONE | ZERO))
		{
			continue;
		}
		else if (i.node->get_leaf_type() & INPUT)
		{
			continue;
		}
		//
		ID_TYPE tmpp = i.node->get_func_id();
		ID_TYPE id = i.eq->get_left_eq() == "" ? VOID_ID : get_id(i.eq->get_left_eq());
		auto ptr = leaf_ids_map.find(id);
		leaf* found_node;
		if(ptr != leaf_ids_map.end())
		{
			found_node = ptr->second;
			i.node->set_left(found_node);
		}
		id = i.eq->get_right_eq() == "" ? VOID_ID : get_id(i.eq->get_right_eq());
		if (id != VOID_ID)
		{
			ptr = leaf_ids_map.find(id);
			if (ptr != leaf_ids_map.end())
			{
				found_node = ptr->second;
				i.node->set_right(found_node);
			}
		}
	}
	//printf("Finished connection\n");
	for (auto &i : all_inputs)
	{
		if (i->check_up_connection())
		{
			unused_leafs.insert(i);
		}
		else
		{
			blnet_o->add_system_leaf(i);
		}
	}
	// Reduce boolnet (delete inessential nodes)
	blnet_o->reduce(1);
	blnet_o->clean_up();
	// Delete after reduce, we don't need these leafs any more
	for (auto &i : const2delete)
	{
		i->mark_to_delete();
		delete i;
	}
}

boolnet_asm::~boolnet_asm()
{
	for (auto &i : simple_func_set)
	{
		delete i;
	}
}

boolnet * boolnet_asm::get_init_bool_net()
{
	return blnet_o;
}

leaf * boolnet_asm::find_leaf_by_id(ID_TYPE id, std::vector<leaf*> leafs_sorted_by_id)
{
	bool is_found = false;
	leaf* found_leaf = nullptr;
	if (leafs_sorted_by_id[0]->get_func_id() == id)
	{
		found_leaf = leafs_sorted_by_id[0];
		is_found = true;
	}
	else
	{
		unsigned int prev_ptr;
		size_t ptr = leafs_sorted_by_id.size() / 2;
		unsigned int left_boundary = 0;
		size_t right_boundary = leafs_sorted_by_id.size();
		while (1)
		{
			prev_ptr = ptr;
			if (leafs_sorted_by_id[ptr]->get_func_id() == id)
			{
				found_leaf = leafs_sorted_by_id[ptr];
				is_found = true;
				break;
			}
			else
			{
				if (leafs_sorted_by_id[ptr]->get_func_id() > id)
				{
					right_boundary = ptr;
					ptr -= (right_boundary - left_boundary) / 2;
					//if (ptr == right_boundary) ptr--;
				}
				else
				{
					left_boundary = ptr;
					ptr += (right_boundary - left_boundary) / 2;
					//if (ptr == left_boundary) ptr++;
				}
			}
			if (prev_ptr == ptr)
			{
				break;
			}
		}
	}
	return found_leaf;
}

ID_TYPE boolnet_asm::get_id(std::string name)
{
	auto ptr = names_map.find(name);
	ID_TYPE id;
	if (ptr == names_map.end())
	{
		id = leaf::get_next_id();
		names_map.insert({ name, id });
		//ids_map.insert({ id, name });
	}
	else
	{
		id = ptr->second;
	}
	return id;
}
