#pragma once
#include <stdlib.h>
#include <vector>
//#include <string>
#include <set>
#include <map>
#include "big_number.h"
#include "k_function.h"
#include "base_node.h"

class DNF_node : public base_node
{
    protected:

		base_node* get_instance_copy(ID_TYPE func_id_);
		k_function k_func;

    public:

		//public methods
		DNF_node(ID_TYPE func_id_, unsigned char status_ = 0);

		//virtual void reduce(int color_);
		void update();

		void set_DNF(std::set<big_number> &DNF_set, std::map<base_node*, int> &DNF_map);

		//std::string get_result_function(std::string(*name_by_id) (ID_TYPE id));
		//std::string get_inv_equal(std::string(*name_by_id) (ID_TYPE id));
		//std::string get_equal(std::string(*name_by_id) (ID_TYPE id));
		//bool is_it_equal(base_node*);
		//bool is_inv_equal(base_node*);

		//virtual void set_translate(base_node* to, bool inv = false);

		//ID_TYPE get_key();
};