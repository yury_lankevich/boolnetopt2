#pragma once
#include <stdlib.h>
#include <vector>
#include <string>
#include <set>
#include <map>
#include "Tokenizer.h"
#include "big_number.h"
#include "k_function.h"

enum leaf_type_e { OUTPUT = 1, INPUT = 2, ONE = 4, MARKED2DELETE = 8, TRANSLATED = 16, EXCLUDED = 32, ZERO = 64, INTERNAL = 0 };
#define SHORT_ID
#ifndef ID_TYPE
#ifdef SHORT_ID
#define ID_TYPE unsigned int
#define VOID_ID 0xffffffff
#define ONE_ID  0xfffffffe
#define ZERO_ID 0xfffffffd
#define FUNC2_TYPE1 0x00055a62
#define FUNC2_TYPE2 0x4055a621
#define FUNC2_TYPE3 0x855a6212
#define FUNC2_TYPE4 0xc5a62123
#define FUNC2_TYPE5 0xe5a62123
#define FUNC2_TYPE_MASK 0x1fffffff
#else
#define ID_TYPE unsigned long long
#define VOID_ID 0xffffffffffffffff
#define ONE_ID  0xfffffffffffffffe
#define ZERO_ID 0xfffffffffffffffd
#define FUNC2_TYPE1 0x000055500aa66220
#define FUNC2_TYPE2 0x40055500aa662201
#define FUNC2_TYPE3 0x8055500aa6622012
#define FUNC2_TYPE4 0xc55500aa66220123
#define FUNC2_TYPE5 0xe55500aa66220123
#define FUNC2_TYPE_MASK 0x1fffffffffffffff
#endif
#endif
/*typedef */class boolnet;
class base_node
{
protected:
	ID_TYPE func_id;

	leaf_type_e status = leaf_type_e::INTERNAL;

	int color = 0;
	bool lock_update = false;

	unsigned int items_beneath_cnt = 0;
	unsigned int linked_down_array_size = 0;
	base_node** linked_down_array;

	unsigned int num_parents = 0;
	unsigned int parents_array_size = 0;
	base_node** linked_up_array;

	//protected methods
	void fields_init();
	virtual base_node* get_instance_copy(ID_TYPE func_id_);

public:
	unsigned level;
	base_node* copy = nullptr;
	bool* inv;
	static ID_TYPE tmp_cnt;
	static ID_TYPE get_next_id();
	static bool without_xor;

	//static std::set<base_node*> all_nodes;

	//public methods
	base_node(ID_TYPE func_id_, unsigned char status_ = 0);
	~base_node();

	virtual base_node* get_copy_leaf(int clr_);
	virtual base_node* get_copy_leaf(int source_clr1_, int target_clr2_, bool change_id);
	void change_id(int source_clr1_, int target_clr2_);
	void set_leaf_type(unsigned char status_);

	bool is_output();

	void get_leaf_set(std::vector<base_node*>& trg_set, int color_);
	void get_inputs(std::vector<base_node*>&, int src_color_, int trg_color_);

	bool check_up_connection(); // if linked_up.size() == 0 return true
	void change_color(int color_);
	void change_color_for_all_parents(int color_);

	void replace(base_node* to, bool inv_ = false);
	void replace_one_by_another(base_node* from, base_node* to, bool inv_);
	virtual void update();
	unsigned update_level(int color_);
	virtual void set_translate(base_node* to, bool inv = false);

	//std::string get_result_function(std::string(*name_by_id) (ID_TYPE id));
	//std::string get_inv_equal(std::string(*name_by_id) (ID_TYPE id));
	//std::string get_equal(std::string(*name_by_id) (ID_TYPE id));
	virtual bool is_it_equal(base_node*); // TODO
	virtual bool is_inv_equal(base_node*); // TODO
	unsigned char get_leaf_type();
	virtual void get_DNF(std::set<big_number>& dnf_set, std::map<base_node*, int>& leaf_map, int color_);

	ID_TYPE get_func_id();

	int get_color();

	void add_linked_up(base_node* linked);
	void delete_linked_up(base_node* linked);
	void mark_to_delete();

	virtual void reduce(int color_);
	virtual ID_TYPE get_key();

	struct comp_id_leaf {
		bool operator()(base_node* a, base_node* b) const
		{
			if (a == b)
			{
				printf("Error: we have to avoid this branch\n");
			}
			if (a == b) return false;
			ID_TYPE id_a = a->get_func_id();
			ID_TYPE id_b = b->get_func_id();
			if (id_a == id_b)
			{
				return a < b;
			}
			return id_a < id_b;
		}
	};
	//typedef 
	struct group_of_nodes
	{
		std::string key;
		std::vector<base_node*> group;
	};
};

