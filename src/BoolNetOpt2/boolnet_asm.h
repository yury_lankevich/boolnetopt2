//#pragma once
#include <stdlib.h>
#include <map>
#include <string>
#include <vector>
#include <set>
#include "leaf.h"
#include "Tokenizer.h"
#include "boolnet.h"
#include "boolnet_equals_organizer.h"

class boolnet_asm
{
public:
	boolnet_asm();
	boolnet_asm(std::vector<std::string> &equals, std::vector<std::string> &in_names_, std::vector<std::string> &out_names_);
	~boolnet_asm();

	void add_equal(std::string);
	void generate_boolnet_by_simple_func_set();

	/*struct comp_long_long {
		bool operator()(std::pair<std::string, ID_TYPE> a, std::pair<std::string, ID_TYPE> b)
		{
			return a.second < b.second;
		}
	};*/

	std::set<leaf*> unused_leafs;
	std::map<std::string, ID_TYPE> names_map;
	//std::map<ID_TYPE, std::string> ids_map;

	boolnet * get_init_bool_net();
protected:
	boolnet* blnet_o;
	std::set<std::string> out_names;
	std::set<std::string> in_names;
	//std::set<boolnet_equals_organizer*> simple_func_set;
	std::vector<boolnet_equals_organizer*> simple_func_set;

	leaf* find_leaf_by_id(ID_TYPE id, std::vector<leaf*> leafs_sorted_by_id);

	ID_TYPE get_id(std::string name);

	struct node_and_eq {
		boolnet_equals_organizer* eq;
		leaf* node;
	};
};