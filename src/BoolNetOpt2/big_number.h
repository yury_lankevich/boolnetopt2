#pragma once
#include <stdlib.h>
#include <vector>
#include <string>
class big_number
{
    protected:
		unsigned int n;
		std::vector<unsigned int> number;
    public: 
		big_number(std::string &str);
		big_number();
		//bool is_neighbor(const big_number& target) const; //Is'n used
		bool crossing(const big_number& target, big_number& result) const;
		bool glew(const big_number& target, big_number& result, bool & partially) const;
		bool absorption(const big_number& target) const;
		char get_code(unsigned int numb) const;
		void set_code(unsigned int num, char code);
		void exclude_pos(unsigned int num);
		bool is_one();
		bool is_one() const;

		void resize(unsigned int);

		int get_number_of_code(char code) const;
		unsigned get_n() const;
		//std::vector<unsigned int> get_number() const;
		bool operator == (const big_number&) const;
		bool operator <  (const big_number&) const;
		big_number operator ^ (const big_number&) const;
		big_number operator & (const big_number&) const;
		bool operator == (const big_number&);
		bool operator <  (const big_number&);
		bool operator >  (const big_number&);
		big_number operator ^ (const big_number&);
		big_number operator & (const big_number&);

		std::vector<big_number> get_zhegalkin_vector(unsigned i);
		std::vector<big_number> get_zhegalkin_vector(unsigned i) const;
		std::string convert2string() const;
};