#include "shannon_path_tree.h"

shannon_path_tree::shannon_path_tree()
{
    
}

shannon_path_tree::~shannon_path_tree()
{
    std::set<shannon_tree_node*> tmp = all_leafs;
    for (auto& i : tmp)
    {
        delete_item(i);
    }
}

void shannon_path_tree::add_item(ID_TYPE id, shannon_tree_node* node)
{
    shannon_tree_node* child = new shannon_tree_node();
    child->id = id;
    child->parent = node;
    if (node == nullptr)
    {
        child->count = 0;
        child_nodes.insert(child);
    }
    else
    {
        child->count = node->count + 1;
        if (max_count < child->count)
        {
            max_count = child->count;
        }
        auto ptr = all_leafs.find(node);
        if (ptr != all_leafs.end())
        {
            all_leafs.erase(ptr);
        }
        node->child_nodes.insert(child);
    }
    all_leafs.insert(child);
}

void shannon_path_tree::add_item(ID_TYPE id, ID_TYPE parent_id)
{
    if (latest_parent_node == nullptr || latest_parent_node->id != parent_id)
    {
        unsigned int max_cnt1 = max_count;
        auto ptr = std::find_if(all_leafs.begin(), all_leafs.end(), [parent_id, max_cnt1](const shannon_tree_node* a) { return (a->id == parent_id) && (a->count >= max_cnt1);  });
        if (ptr == all_leafs.end())
        {
             printf("[Error][shannon_path_tree::add_item] unexpected parent_id");
             return;
        }
        else
        {
            latest_parent_node = (*ptr);
        }
    }
    add_item(id, latest_parent_node);
}

void shannon_path_tree::delete_item(shannon_tree_node* node)
{
    auto ptr = all_leafs.find(node);
    if (ptr != all_leafs.end())
    {
        all_leafs.erase(ptr);
    }
    if (node->parent != nullptr)
    {
        node->parent->child_nodes.erase(std::find(node->parent->child_nodes.begin(), node->parent->child_nodes.end(), node));
        if (node->parent->child_nodes.size() == 0)
        {
            delete_item(node->parent);
        }
        else
        {
            max_count = node->parent->count + 1;
        }
    }
    else
    {
        ptr = child_nodes.find(node);
        child_nodes.erase(ptr);
        max_count = 0;
    }
    delete node;
}

void shannon_path_tree::delete_item(ID_TYPE id)
{
    unsigned int max_cnt1 = max_count;
    auto ptr = std::find_if(all_leafs.begin(), all_leafs.end(), [id, max_cnt1](const shannon_tree_node* a) { return (a->id == id) && (a->count >= max_cnt1);  } );
    auto node = *ptr;
    all_leafs.erase(ptr);
    if (node->parent != nullptr)
    {
        node->parent->child_nodes.erase(std::find(node->parent->child_nodes.begin(), node->parent->child_nodes.end(), node));
        if (node->parent->child_nodes.size() == 0)
        {
            delete_item(node->parent);
        }
    }
    else
    {
        ptr = std::find_if(child_nodes.begin(), child_nodes.end(), [id](const shannon_tree_node* a) { return a->id == id;  });
        child_nodes.erase(ptr);
    }
    delete node;
}

void shannon_path_tree::reset_to_root()
{
    latest_parent_node = nullptr;
}

ID_TYPE shannon_path_tree::get_next()
{
    if (latest_parent_node == nullptr)
    {
        if (child_nodes.size() > 0)
        {
            for (auto& i : child_nodes)
            {
                if (i->child_nodes.size() > 0)
                {
                    latest_parent_node = i;
                    return i->id;
                }
            }
            latest_parent_node = *(child_nodes.begin());
        }
        else
        {
            return VOID_ID;
        }
    }
    else
    {
        for (auto& i : latest_parent_node->child_nodes)
        {
            if (i->child_nodes.size() > 0)
            {
                latest_parent_node = i;
                return i->id;
            }
        }
        if (latest_parent_node->child_nodes.size() > 0)
        {
            latest_parent_node = *(latest_parent_node->child_nodes.begin());
        }
    }
    return latest_parent_node->id;
}

unsigned int shannon_path_tree::get_size()
{
    return (unsigned int) all_leafs.size();
}

bool shannon_path_tree::is_empty()
{
    return all_leafs.size() == 0;
}
