#include "boolnet_processor.h"

boolnet_processor::boolnet_processor(sf sf0_, input_param param_)
{
	param = param_;
	sf0 = sf0_;

	boolnet::type = param.type;
	boolnet::names_map = new std::map<std::string, ID_TYPE>();
	boolnet::ids_map = new std::map<ID_TYPE, std::string>();

	for (auto& i : sf0.inSF)
	{
		leaf2use.insert(get_id(i));
	}
	for (auto& i : sf0.outSF)
	{
		get_id(i);
	}

	std::set<leaf*> unused_leafs;
	net = get_init_bool_net(sf0, unused_leafs);

	if (param.order != nullptr)
	{
		base_node::without_xor = !param.xor_;
		boolnet *result_net = get_net_by_specific_order(net, param.order, unused_leafs, sf0);
		result_equals = result_net->convert2string();
		delete result_net;
		done = true;
	}
	else if (param.reduction_only) {
		base_node::without_xor = !param.xor_;
		net->reduce();
		net->clean_up();
		result_equals = net->convert2string();
		done = true;
	}
	else
	{
		for (auto& i : unused_leafs)
		{
			leaf2use.erase(i->get_func_id());
			delete i;
		}
		omp_set_nested(1); // разрешить вложенный параллелизм

		min_cnt = net->get_nodes_cnt();
		//min_cnt = 0x7fffffff;
		result_equals = net->convert2string();
		//net->make_DNF_nodes();
	}
}

boolnet_processor::~boolnet_processor()
{
	if(net != nullptr)
		delete net;
	delete boolnet::names_map;
	delete boolnet::ids_map;
}

ID_TYPE boolnet_processor::get_id(std::string name)
{
	auto ptr = boolnet::names_map->find(name);
	ID_TYPE id;
	if (ptr == boolnet::names_map->end())
	{
		id = base_node::get_next_id();
		boolnet::names_map->insert({ name, id });
		boolnet::ids_map->insert({ id, name });
	}
	else
	{
		id = ptr->second;
	}
	return id;
}

bool boolnet_processor::get_done()
{
	return done;
}

boolnet* boolnet_processor::get_init_bool_net(sf sf0, std::set<leaf*>& unused_leafs)
{
	boolnet_asm bool_net_asm_obj(sf0.m_in, sf0.inSF, sf0.outSF);
	unused_leafs = bool_net_asm_obj.unused_leafs;
	return bool_net_asm_obj.get_init_bool_net();
}

boolnet* boolnet_processor::shannon_expansion(boolnet* net, ID_TYPE id)
{
	boolnet* new_net;
	//printf("Shannon exp started...\n");
#pragma omp critical
	{
		new_net = net->get_copy();
	}
	new_net->shannon_expansion(id);
	return new_net;
	//printf("Shannon exp finished...\n");
}

boolnet* boolnet_processor::get_net_by_specific_order(boolnet* net, std::string order, std::set<leaf*>& unused_leafs, sf sf0)
{
	std::vector<std::string> order_vector;
	CTokenizer<CIsChar<' '>>::Tokenize(order_vector, order);


	for (auto& i : unused_leafs)
	{
		order_vector.erase(std::find(order_vector.begin(), order_vector.end(), boolnet::get_name_by_id(i->get_func_id())));
		delete i;
	}

	ID_TYPE cur_id;
	std::vector<std::string> equals;
	for (auto i = order_vector.begin(); i != order_vector.end() - 1; i++)
	{
		cur_id = get_id(*i);
		net->shannon_expansion(cur_id);
		std::set<std::string> tmp = net->get_merge_result();
		for (auto& kj : tmp)
		{
			equals.push_back(kj);
		}
		net->clean_merge_result();
	}
	{
		std::vector<std::string>  tmp = net->convert2string(false);
		for (auto& kj : tmp)
		{
			equals.push_back(kj);
		}
	}
	base_node::tmp_cnt = (unsigned int) boolnet::names_map->size(); // ID should be greater then name size to avoid collosions
	boolnet_equals_organizer::tmp_cnt = 0;
	base_node::without_xor = true;
	printf("Start to build boolnet from string\n");
	boolnet_asm bool_net_asm_obj(equals, sf0.inSF, sf0.outSF);
	boolnet* net_tmp = bool_net_asm_obj.get_init_bool_net();
	printf("Start to reduce complete boolnet\n");
	net_tmp->reduce();
	net_tmp->clean_up();
	return net_tmp;
}

boolnet* boolnet_processor::get_net_from_equals(std::vector<std::string> eq)
{
	base_node::tmp_cnt = (unsigned int) boolnet::names_map->size(); // ID should be greater then name size to avoid collisions
	boolnet_equals_organizer::tmp_cnt = 0;
	base_node::without_xor = true;
	//printf("Start to build boolnet from string\n");
	boolnet_asm bool_net_asm_obj(eq, sf0.inSF, sf0.outSF);
	boolnet* n = bool_net_asm_obj.get_init_bool_net();
	//printf("Start to reduce complete boolnet\n");
	n->reduce();
	n->clean_up();
	return n;
}

bool boolnet_processor::get_result()
{
	if (done) return true;

	std::vector<ID_TYPE> next_inp_set, current_inp_set;
	for (auto& i : leaf2use)
	{
		current_inp_set.push_back(i);
	}

	//unsigned int cnt = 0;
	boolnet* min_net = nullptr;
	boolnet* min_shannon_path_net = nullptr;

	std::string order;
	std::vector<std::string> equals;
	ID_TYPE prev_min_inp = 0;

	shpth_tree.reset_to_root();
	if (!shpth_tree.is_empty())
	{
		do
		{
			auto ptr = std::find(current_inp_set.begin(), current_inp_set.end(), shpth_tree.get_next());
			if (ptr == current_inp_set.end()) break;
			prev_min_inp = *ptr;
			order += " " + boolnet::get_name_by_id(prev_min_inp);
			// TODO: pre save instead?
			net->shannon_expansion(*ptr);
			current_inp_set.erase(ptr);
			std::set<std::string> tmp = net->get_merge_result();
			for (auto& kj : tmp)
			{
				equals.push_back(kj);
			}
			net->clean_merge_result();
		} while (true);
		shannon_path_iter++;
	}
	else
	{
		//Let's try random
		/*if (rand_on)
		{
			std::vector<ID_TYPE> rand_order;
			std::srand(static_cast<unsigned int>(time(0)));
			printf("rand\n");
			do
			{
				unsigned int rand_pos;
				rand_order.clear();
				order.clear();
				while (current_inp_set.size() > 1)
				{
					rand_pos = std::rand() % current_inp_set.size();
					rand_order.push_back(current_inp_set[rand_pos]);
					order += " " + boolnet::get_name_by_id(rand_order.back());
					current_inp_set.erase(current_inp_set.begin() + rand_pos);
				}
				current_inp_set.clear();
				for (auto& i : leaf2use)
				{
					current_inp_set.push_back(i);
				}
				if (prev_order_set.find(order) != prev_order_set.end()) continue;
				order.clear();

				for (auto& i : rand_order)
				{
					net->shannon_expansion(i);
					std::set<std::string> tmp = net->get_merge_result();
					for (auto& kj : tmp)
					{
						equals.push_back(kj);
					}
					net->clean_merge_result();
				}
				{
					std::set<std::string> tmp = net->get_all_roots();
					for (auto& kj : tmp)
					{
						equals.push_back(kj);
					}
				}
				delete net;
				net = get_net_from_equals(equals);
				equals.clear();
				break;
			} while (true);
			rand_on = false;
		}*/
		shannon_path_iter = 0;
	}

	unsigned int min_net_size = 0;
	while (current_inp_set.size() > 1)
	{
		ID_TYPE min_inp = 0;
		unsigned int done_cnt = 0;
		base_node::without_xor = false | !param.xor_;
		std::vector<ID_TYPE> eq_inp_set;
#pragma omp parallel num_threads(4)
		{
#pragma omp single
			{
				//#pragma omp for schedule(dynamic, 4)
				for (int j = 0; j < current_inp_set.size(); j++)
				{
#pragma omp task /*untied*/ firstprivate(j)
					{
						ID_TYPE i;
						boolnet* expanded_net;
#pragma omp critical
						{
							i = current_inp_set[j];
							expanded_net = net->get_copy();
						}
						expanded_net->shannon_expansion(i);
#pragma omp critical
						{
							//printf(" %0d (%0d): ", ++done_cnt, current_inp_set.size());
							if (min_net != nullptr)
							{
								unsigned int exp_net_size = expanded_net->get_nodes_cnt();
								//printf("EXP_NET size %0d\n", exp_net_size);
								/*if (exp_net_size == min_net_size && i < min_inp) // to get same result
								{
									boolnet* to_del = min_net;
									next_inp_set.push_back(min_inp);
									min_net = expanded_net;
									min_inp = i;
									expanded_net = to_del;
									if (exp_net_size <= 1 || current_inp_set.size() <= 2)
									{
										eq_inp_set.clear();
									}
									eq_inp_set.push_back(min_inp);
								}
								else*/ if (exp_net_size < min_net_size)
								{
									boolnet* to_del = min_net;
									next_inp_set.push_back(min_inp);
									min_net = expanded_net;
									min_net_size = exp_net_size;
									min_inp = i;
									expanded_net = to_del;
									eq_inp_set.clear();
									eq_inp_set.push_back(min_inp);
								}
								else
								{
									next_inp_set.push_back(i);
									if (exp_net_size == min_net_size && exp_net_size > 1 && current_inp_set.size() > 2 && shannon_path_iter < 10 && shpth_tree.get_size() < 10)
									{
										eq_inp_set.push_back(i);
									}
								}
							}
							else
							{
								min_net = expanded_net;
								min_net_size = min_net->get_nodes_cnt();
								//printf("EXP_NET size %0d\n", min_net_size);
								expanded_net = nullptr;
								min_inp = i;
								eq_inp_set.clear();
								eq_inp_set.push_back(min_inp);
							}
						}
						if (expanded_net != nullptr)
						{
							delete expanded_net;
						}
					}
					//printf("\n%d\n", cnt++);
				}
#pragma omp taskwait
			}
		}
		//#pragma omp barrier

		//std::sort(eq_inp_set.begin(), eq_inp_set.end()); // to get same result
		if (shpth_tree.is_empty())
		{
			for (auto& i : eq_inp_set)
			{
				shpth_tree.add_item(i);
			}
		}
		else
		{
			for (auto& i : eq_inp_set)
			{
				shpth_tree.add_item(i, prev_min_inp);
			}
		}
		prev_min_inp = min_inp;

		order += " " + boolnet::get_name_by_id(min_inp);
		//TODO: update result net
		std::set<std::string> tmp = min_net->get_merge_result();
		for (auto& kj : tmp)
		{
			equals.push_back(kj);
		}
		if (next_inp_set.size() == 1)
		{
			tmp = min_net->get_all_roots();
			for (auto& kj : tmp)
			{
				equals.push_back(kj);
			}
		}
		delete net;
		net = min_net;
		min_net = nullptr;
		current_inp_set = next_inp_set;
		next_inp_set.clear();
		//printf("%d\n", cnt++);
	}
	if (min_net_size != 0) // We can remove this
	{
		auto tmp_eq = net->convert2string(false);
		equals.insert(equals.end(), tmp_eq.begin(), tmp_eq.end());
	}
	delete net;
	shpth_tree.delete_item(prev_min_inp);
	if (prev_order_set.find(order) != prev_order_set.end())
	{
		net = nullptr;
		done = true;
		printf("result order %s\n", res_order.data());
	}
	else
	{
		net = get_net_from_equals(equals);
		printf("NODE_CNT %0d order %s\n", net->get_nodes_cnt(), order.data());
		prev_order_set.insert(order);

		if (!shpth_tree.is_empty()) // to get same result
		{
			if (min_shannon_path_net == nullptr)
			{
				min_shannon_path_net = net->get_copy();
			}
			else
			{
				if (min_shannon_path_net->get_nodes_cnt() > net->get_nodes_cnt())
				{
					delete min_shannon_path_net;
					min_shannon_path_net = net->get_copy();
				}
			}
		}
		else
		{
			if (min_shannon_path_net != nullptr)
			{
				delete net;
				net = min_shannon_path_net;
				min_shannon_path_net = nullptr;
			}
		}
		unsigned int tmp_cnt = net->get_nodes_cnt();
		if (tmp_cnt < min_cnt)
		{
			min_cnt = tmp_cnt;
			result_equals = net->convert2string();
			res_order = order;
			not_so_good_cnt = 0;
			return true;
		}
		/*else if (not_so_good_cnt > 10)
		{
			rand_on = true;
			not_so_good_cnt = 0;
		}
		else// if(shpth_tree.is_empty())
		{
			not_so_good_cnt++;
		}
		printf("not so good count = %0d\n", not_so_good_cnt);*/
	}
	return done;
}