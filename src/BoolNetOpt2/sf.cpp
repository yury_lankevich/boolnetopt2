#include "sf.h"
sf::sf(char * fname)
{
	ReadSf(fname);
}
sf::sf()
{
}
void sf::ReadSf(char * fname)
{
	std::ifstream ins1; //open text1.txt for input
 	std::vector<std::string> oResult; 
	std::string linIn;
	std::string linOut;
	std::string linIt;
	bool fDCL_PIN = false;
 //   std::string tit;
	std::vector<std::string> itSF;

   ins1.open (fname); //opens file, exit if fail
  if(ins1.fail()) { //if opening fails then end program
	  printf("file openning is fail");
	  exit(-1);
    //return; //exit function from cstdlib library
  }
  std::string lin;
  int i = 0;
  if(ins1.is_open()) {
    while(! ins1.eof()) {

      getline(ins1, lin, '\n');
	  CTokenizer<>::Tokenize(oResult,lin);
      if (oResult.size() && oResult[0] == "TITLE") name = oResult[1];
	  if (oResult.size() && oResult[0] == "DCL_PIN") fDCL_PIN = true;
	  if (oResult.size() && oResult[0] == "END_PIN") fDCL_PIN = false;
	  if (fDCL_PIN && oResult.size() && oResult[0] == "INP") {
		  do {getline(ins1, lin, '\n');
		  CTokenizer<>::Tokenize(oResult, lin);
		  if (oResult.size() > 0)
		  {
			  if (oResult[0] == "OUT") break;
			  linIn += " " + lin;
		  }
		  }while(1);
		CTokenizer<>::Tokenize(oResult,linIn);
		for (auto &tmp_in : oResult)
		{
			inSF.push_back(tmp_in);
		}
		  do {getline(ins1, lin, '\n');
		  CTokenizer<>::Tokenize(oResult, lin);
		  if (oResult.size() > 0)
		  {
			  if (oResult[0] == "INTER") break;
			  linOut += " " + lin;
		  }
		  }while(1);
		CTokenizer<>::Tokenize(oResult,linOut);
		for (auto &tmp_in : oResult)
		{
			outSF.push_back(tmp_in);
		}
		  do {getline(ins1, lin, '\n');
		  CTokenizer<>::Tokenize(oResult, lin);
		  if (oResult.size() > 0)
		  {
			  if (oResult[0] == "END_PIN") break;
			  linIt += " " + lin;
		  }
		  }while(1);
		  if (linIt.size()){
		CTokenizer<>::Tokenize(itSF,linIt);
		//inSF.insert(inSF.end(), itSF.begin(), itSF.end());
		//outSF.insert(outSF.end(), itSF.begin(), itSF.end());
		  }
		continue;
	  }

	  if (oResult.size() && oResult[0] == "FUNCTION") 
	  {
			getline(ins1, lin, '\n');
			CTokenizer<>::Tokenize(oResult, lin);
			if (oResult[0] != "LOG") return;
			getline(ins1, lin, '\n'); 
			
			getline(ins1, lin, '\n');
			CTokenizer<>::Tokenize(oResult, lin);
			while(oResult[0] != "END_LOG")
			{
				std::string tmp_str = "";
				bool end_of_eq = false;
				do {
					CTokenizer <CIsChar<' '>>::Tokenize(oResult, lin);
					for (auto &stri : oResult)
					{
						tmp_str += stri;
					}
					end_of_eq = tmp_str[tmp_str.length() - 1] == ';';
					if (end_of_eq) break;
					getline(ins1, lin, '\n');
				}
				while (true);

				CTokenizer <CIsChar<';'>>::Tokenize(oResult, tmp_str);
				if(oResult.size() > 1) printf("Error (sf.cpp:70): unexpected input: %s", lin.data()), exit(1);
				m_in.push_back(oResult[0]);
				//TODO: 
				getline(ins1, lin, '\n');
				CTokenizer<>::Tokenize(oResult, lin);
			}
		}
	}
   ins1.close();
 }

}
void sf::SaveSf(char * fname, std::vector<std::string> &functions)
{
	std::string path = fname;
	bool is_dir = false;
	for( auto i = path.end() -2; i != path.begin(); i--)
	{
		if( (*i == '/') || (*i == '\\') )
		{
			path.erase(i, path.end());
			is_dir = true;
			break;
		}
	}
	if(is_dir)
	{
#ifdef VS_COMPILER
		std::filesystem::path dir(path);
		std::filesystem::create_directories(dir);
#else
		boost::filesystem::path dir(path);
		boost::filesystem::create_directories(dir);
#endif // VS_COMPILER
	}
	FILE* f;
	fopen_s(&f, fname, "w");
	fprintf(f, "TITLE %s\n\n",name.data());
	fprintf(f, "FORMAT SF\n");
	fprintf(f, "AUTHOR Lit\n"); //TODO: ??
	fprintf(f, "DATE 1-27-2000\n"); //TODO: get data
	fprintf(f, "PROJECT BenchMark\n\n"); //TODO: ??
	fprintf(f, "DCL_PIN\n");
	fprintf(f, "EXT\n");
	fprintf(f, "INP\n");
	{
		std::string inputs;
		for(auto &i : inSF)
		{
			inputs += i + " ";
			if(inputs.size() >= 30)
			{
				fprintf(f, "%s\n", inputs.data());
				inputs.clear();
			}
		}
		if(inputs.size() != 0) fprintf(f, "%s\n", inputs.data());
	}
	fprintf(f, "OUT\n");
	{
		std::string outs;
		for(auto &i : outSF)
		{
			outs += i + " ";
			if(outs.size() >= 30)
			{
				fprintf(f, "%s\n", outs.data());
				outs.clear();
			}
		}
		if(outs.size() != 0) fprintf(f, "%s\n", outs.data());
	}
	fprintf(f, "INTER\n");
	fprintf(f, "END_PIN\n\n");
	fprintf(f, "FUNCTION\n");
	fprintf(f, "LOG\n");
	fprintf(f, "%d %d 0\n", (int) inSF.size(), (int) outSF.size());
	for(auto &i : functions)
		fprintf(f, "%s\n", i.data());
	fprintf(f, "END_LOG\n");
	fprintf(f, "END_FUNCTION\n");
	fprintf(f, "END_%s\n",name.data());
	fclose(f);
}