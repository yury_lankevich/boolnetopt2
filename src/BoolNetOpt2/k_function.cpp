#include "k_function.h"

k_function::k_function()
{
	zhegalkin = false;
}
void k_function::exclude_pos(std::vector<unsigned int>& positions)
{
	std::set<big_number> new_conj_set;
	for (auto& i : conjunctions)
	{
		big_number tmp = i;
		for (auto& pos : positions)
		{
			tmp.exclude_pos(pos);
		}
		new_conj_set.insert(tmp);
	}
	conjunctions = new_conj_set;
}
void k_function::exclude_pos(std::vector<unsigned int>& positions, std::vector<char> codes)
{
	std::set<big_number> new_conj_set;
	for (auto& i : conjunctions)
	{
		bool dont_add = false;
		big_number tmp = i;
		auto code = codes.begin();
		for (auto& pos : positions)
		{
			if (0 == (*code & tmp.get_code(pos)) )
			{
				dont_add = true;
				break;
			}
			tmp.exclude_pos(pos);
			code++;
		}
		if (!dont_add)
		{
			new_conj_set.insert(tmp);
		}
	}
	conjunctions = new_conj_set;
}
k_function::k_function(std::vector<std::string> &m_in, bool it_is_zh,std::string fname)
{
	zhegalkin = false;
	for( auto &i : m_in)	{
		big_number temp(i);
		conjunctions.insert(temp);
	}
	if (it_is_zh) 
		to_zhegalkin_polinom(fname);
	else
		minimization();
}

bool k_function::is_one()
{
	if (!zhegalkin)	{
		for (auto &i : conjunctions)	{
			if (i.is_one()) return true;
		}
		return false;
	}
	else	{
		if (zhegalkin_polinom.size() == 1)		{
			return zhegalkin_polinom.begin()->is_one();
		}
		else	{
			return false;
		}
	}
}

big_number k_function::get_descriptor()
{
	big_number descriptor = *(conjunctions.begin());
	auto i = conjunctions.begin();
	i++;
	for ( ; i!= conjunctions.end(); i++)	{
		descriptor = descriptor & (*i);
	}
	return descriptor;
}

void k_function::add_conjunction(big_number& new_conjunction)
{
	if (zhegalkin)
	{
		printf("Unexpected call\n");
		zhegalkin_polinom.push_back(new_conjunction);
	}
	else
	{
		conjunctions.insert(new_conjunction);
	}
}

void k_function::add_conjunction(std::set<big_number>& conj_set)
{
	conjunctions.insert(conj_set.begin(), conj_set.end());
}

void k_function::minimization()
{
	if(conjunctions.size() < 2) return;
	std::set<big_number > tmp;
	big_number            tmp_big;
	bool is_changed = false;
	int iter = 0;
    // ��������� �� ������ �� ���������� ��������
	std::vector<std::set<big_number>> layered_conj, additional_layers;
	for (auto &i : conjunctions)	{
		bool found_the_group = false;
		int number_of_code_0x3 = i.get_number_of_code(0x3);
		for (auto &jj : layered_conj)	{
			if (jj.begin()->get_number_of_code(0x3) == number_of_code_0x3)	{
				jj.insert(i);
				found_the_group = true;
				break;
			}
		}
		if (!found_the_group)	{
			std::set<big_number> tmpl;
			tmpl.insert(i);
			layered_conj.push_back(tmpl);
		}
	}

	std::sort(layered_conj.begin(), layered_conj.end(), [](std::set<big_number> &a, std::set<big_number> &b) {
		return a.begin()->get_number_of_code(0x3) < b.begin()->get_number_of_code(0x3);
	});
	//���� �������
	for (auto jj = layered_conj.begin(); jj != layered_conj.end(); jj++)	{
		auto next_jj = jj;
		next_jj++;
		auto next_layer = next_jj;
		auto current_layer = jj;
		bool current_jj = true;
		do	{
			if (current_layer->size() == 1) break;
			if ((next_jj == layered_conj.end()) ||
				(next_jj->begin()->get_number_of_code(0x3) != current_layer->begin()->get_number_of_code(0x3) + 1))		{
				std::set<big_number> tmpl;
				additional_layers.push_back(tmpl);
				next_layer = additional_layers.end();
				next_layer--;
				if (!current_jj)	{
					current_layer = next_layer;
					current_layer--;
				}
			}
			else	{
				next_layer = next_jj;
			}
			tmp.clear();
			is_changed = false;
			std::set<big_number> already_is_set;
			//�� Current � ������ �� ���������� 1
			std::vector<std::set<big_number>> group_conj_by_ones;
			for (auto &ii : (*current_layer))	{
				bool found_the_group = false;
				int number_of_code_0x2 = ii.get_number_of_code(0x2);
				for (auto &jjj : group_conj_by_ones)	{
					if (jjj.begin()->get_number_of_code(0x2) == number_of_code_0x2)	{
						jjj.insert(ii);
						found_the_group = true;
						break;
					}
				}
				if (!found_the_group)	{
					std::set<big_number> tmpl;
					tmpl.insert(ii);
					group_conj_by_ones.push_back(tmpl);
				}
			}
			std::sort(group_conj_by_ones.begin(), group_conj_by_ones.end(), [](std::set<big_number> &a, std::set<big_number> &b) {
				return a.begin()->get_number_of_code(0x2) < b.begin()->get_number_of_code(0x2);
			});
			//��������� ������ ���� ����������� �� ���������� ������ �� 1
			for (auto ig = group_conj_by_ones.begin(); ig != group_conj_by_ones.end(); ig++)	{
				unsigned int j = 0;
				auto ig_next = ig;
				ig_next++;
				if (ig_next == group_conj_by_ones.end())	{
					for (auto &i : (*ig))	{
						tmp.insert(i);
					}
					break;
				}
				if (ig_next->begin()->get_number_of_code(0x2) != ig->begin()->get_number_of_code(0x2) + 1)	{
					for (auto &i : (*ig))	{
						tmp.insert(i);
					}
					continue;
				}
				for (auto &i : (*ig))	{
					bool already_is = false;
					if (already_is_set.find(i) != already_is_set.end())	{
						already_is = true;
					}
					for (auto &ii : (*ig_next))		{
						bool partially;
						if (already_is)		{
							if (already_is_set.find(ii) != already_is_set.end())	{
								continue;
							}
						}
						if (i.glew(ii, tmp_big, partially))	{
							if (!partially)		{
								next_layer->insert(tmp_big);
								is_changed = true;
								already_is_set.insert(ii);
								already_is = true;
							}
							else	{
								if (already_is_set.find(ii) == already_is_set.end())	{
									printf("\nk_function: Error: must not be here\n");
									printf("%s + %s = %s\n", i.convert2string().data(), ii.convert2string().data(), tmp_big.convert2string().data());
									i.glew(ii, tmp_big, partially);
									tmp.insert(tmp_big);
									is_changed = true;
									already_is_set.insert(ii);
								}
							}
						}
					}
					if (!already_is)	{
						tmp.insert(i);
					}
				}
			}
			
			if (is_changed)	{
				//�������� ����� �������� ������������
				if (current_layer->size() < tmp.size())
				{
					printf("\nk_function: Error: must not be here\n");
				}
				(*current_layer) = tmp;
			}
			if (next_layer->size() == 0) break;
			current_jj = false;
			current_layer = next_layer;
		} while (current_layer != next_jj);
	}
	layered_conj.insert(layered_conj.begin(), additional_layers.begin(), additional_layers.end());
	additional_layers.clear();
	std::sort(layered_conj.begin(), layered_conj.end(), [](std::set<big_number> &a, std::set<big_number> &b) {
		if (a.size() == 0) return false;
		if (b.size() == 0) return true;
		return a.begin()->get_number_of_code(0x3) < b.begin()->get_number_of_code(0x3);
	});
	//���� ����������
	for (auto jj = layered_conj.begin(); jj != layered_conj.end(); jj++)	{ //TODO: reverse the order... it may encrease speed
		is_changed = false;
		tmp.clear();
		if (jj->size() == 0) break;
		auto next_jj = jj;
		next_jj++;
		{
			unsigned int j = 0;
			for (auto i = jj->begin(); j < jj->size(); i++, j++)		{
				bool already_is = false;
				for (auto ll = next_jj; ll != layered_conj.end(); ll++)		{
					for (auto ii = ll->begin(); ii != ll->end(); ii++)			{
						if (i->absorption(*ii))			{
							is_changed = true;
							already_is = true;
							break;
						}
					}
					if (already_is) break;
				}
				if (!already_is)		{
					tmp.insert(*i);
				}
			}
			if (is_changed)		{
				(*jj) = tmp;
			}
		}
	}
	std::sort(layered_conj.begin(), layered_conj.end(), [](std::set<big_number> &a, std::set<big_number> &b) {
		if (a.size() == 0) return false;
		if (b.size() == 0) return true;
		return a.begin()->get_number_of_code(0x3) < b.begin()->get_number_of_code(0x3);
	});
	//���� �����������
	for (auto jj = layered_conj.begin(); jj != layered_conj.end(); jj++)	{
		auto next_jj = jj;
		next_jj++;
		auto next_layer = next_jj;
		auto current_layer = jj;
		bool current_jj = true;
		bool additional_layer_in_ise = false;
		do	{
			if ((next_jj == layered_conj.end()) || (next_jj->size() != 0))		{
				if ((next_jj == layered_conj.end()) ||
					(next_jj->begin()->get_number_of_code(0x3) != current_layer->begin()->get_number_of_code(0x3) + 1))		{
					std::set<big_number> tmpl;
					additional_layers.push_back(tmpl);
					next_layer = additional_layers.end();
					next_layer--;
					additional_layer_in_ise = true;
					if (!current_jj)	{
						current_layer = next_layer;
						current_layer--;
					}
				}
				else	{
					next_layer = next_jj;
					additional_layer_in_ise = false;
				}
			}
			else	{
				next_layer = next_jj;
				additional_layer_in_ise = false;
			}
			tmp.clear();
			is_changed = false;
			unsigned int j = 0;
			std::set<big_number> already_is_set;
			//�� Current � ������ �� ���������� 1
			std::vector<std::set<big_number>> group_conj_by_ones;
			for (auto &ii : (*current_layer))	{
				bool found_the_group = false;
				int number_of_code_0x2 = ii.get_number_of_code(0x2);
				for (auto &jjj : group_conj_by_ones)	{
					if (jjj.begin()->get_number_of_code(0x2) == number_of_code_0x2)		{
						jjj.insert(ii);
						found_the_group = true;
						break;
					}
				}
				if (!found_the_group)	{
					std::set<big_number> tmpl;
					tmpl.insert(ii);
					group_conj_by_ones.push_back(tmpl);
				}
			}
			std::sort(group_conj_by_ones.begin(), group_conj_by_ones.end(), [](std::set<big_number> &a, std::set<big_number> &b) {
				return a.begin()->get_number_of_code(0x2) < b.begin()->get_number_of_code(0x2);
			});
			//��������� ������ ���� ����������� �� ���������� ������ �� 1
			for (auto ig = group_conj_by_ones.begin(); ig != group_conj_by_ones.end(); ig++)
			{
				unsigned int j = 0;
				bool glew_isnt_possible = false;
				auto ig_next = ig;
				ig_next++;
				if (ig_next == group_conj_by_ones.end())	{
					glew_isnt_possible = true;
				}
				else
					if (ig_next->begin()->get_number_of_code(0x2) != ig->begin()->get_number_of_code(0x2) + 1)		{
						glew_isnt_possible = true;
					}
				for (auto &i : (*ig))	{
					bool already_is = false;
					if (already_is_set.find(i) != already_is_set.end())		{
						already_is = true;
					}
					if (!glew_isnt_possible)	{
						for (auto &ii : (*ig_next))		{
							bool partially;
							if (already_is)	{
								if (already_is_set.find(ii) != already_is_set.end())	{
									continue;
								}
							}
							if (i.glew(ii, tmp_big, partially))		{
								if (!partially)		{
									next_layer->insert(tmp_big);
									is_changed = true;
									already_is_set.insert(ii);
									already_is = true;
								}
								else	{
									if (already_is_set.find(ii) == already_is_set.end())	{
										printf("\nk_function: Error: must not be here\n");
										printf("%s + %s = %s\n", i.convert2string().data(), ii.convert2string().data(), tmp_big.convert2string().data());
										i.glew(ii, tmp_big, partially);
										tmp.insert(tmp_big);
										is_changed = true;
										already_is_set.insert(ii);
									}
								}
							}
						}
					}
					if (!already_is)	{
						if (already_is_set.find(i) == already_is_set.end())		{
							glew_in_the_layer(i, tmp_big, *next_layer, *next_layer, is_changed, already_is, true);
							if ((next_jj != layered_conj.end()) && !already_is)		{
								//bool check = additional_layer_in_ise || (next_layer != next_jj);
								for (auto ll = additional_layer_in_ise ? next_jj : next_jj + 1; ll != layered_conj.end(); ll++)	{
									glew_in_the_layer(i, tmp_big, *next_layer, *ll, is_changed, already_is);
									if (already_is) break;
								}

							}
							if (!already_is)	tmp.insert(i);
						}
					}
				}
			}
			if (is_changed)
			{
				(*current_layer) = tmp;
				tmp.clear();
				{
					bool is_changed_a = false;
					if (next_layer->size() != 0)	{
						unsigned int ja = 0;
						for (auto i = current_layer->begin(); ja < current_layer->size(); i++, ja++)	{
							bool already_is_a = false;
							for (auto &ii : (*next_layer))		{
								if (i->absorption(ii))		{
									is_changed_a = true;
									already_is_a = true;
									break;
								}
							}
							if (!already_is_a)		{
								tmp.insert(*i);
							}
						}
						if (is_changed_a)	{
							(*current_layer) = tmp;
						}
					}
				}
			}
			if (next_layer->size() == 0) break;
			current_jj = false;
			current_layer = next_layer;
		} while (additional_layer_in_ise);
	}
	layered_conj.insert(layered_conj.begin(), additional_layers.begin(), additional_layers.end());
	std::sort(layered_conj.begin(), layered_conj.end(), [](std::set<big_number> &a, std::set<big_number> &b) {
		if (a.size() == 0) return false;
		if (b.size() == 0) return true;
		return a.begin()->get_number_of_code(0x3) < b.begin()->get_number_of_code(0x3);
	});
	tmp.clear();
	for (auto &jj : layered_conj)	{
		for (auto &ii : jj)
			tmp.insert(ii);
	}
	if (conjunctions.size() < tmp.size()) 		printf("\nk_function: Error: must not be here\n");
	conjunctions = tmp;
}

bool k_function::there_is_inputs(unsigned pos, char val)
{
	bool inv_res = val == 0x3;
	for(auto &i: conjunctions)	{
		if(inv_res != (i.get_code(pos) == val)) return true;
	}
	return false;
}

void k_function::glew_in_the_layer(const big_number &i, big_number &tmp_big, std::set<big_number> &next_layer, std::set<big_number> &ll, bool &is_changed, bool &already_is, bool glew_on_err)
{
	for (auto ii = ll.begin(); ii != ll.end(); ii++) {
		bool partially;
		if (i.glew((*ii), tmp_big, partially)) {
			if (!partially) {
				is_changed = true;
				already_is = true;
				if (i.absorption((*ii))) {
					break;
				}
				else {
					printf("\nk_function: Error: must not be here\n");
					if (glew_on_err)
					{
						i.glew((*ii), tmp_big, partially);
					}
					printf("\nResult of patrially glew:\n");
					printf("%s + %s = %s\n", i.convert2string().data(), ii->convert2string().data(), tmp_big.convert2string().data());
					next_layer.insert(tmp_big);
				}
			}
			else {
				next_layer.insert(tmp_big);
				already_is = true;
				is_changed = true;
				break;
			}
		}
	}
}

size_t k_function::size()
{
	if (zhegalkin)	{
		return zhegalkin_polinom.size();
	}
	else	{
		return conjunctions.size();
	}
}

void k_function::to_zhegalkin_polinom(std::string fname)
{
	zhegalkin = true;
	big_number tmp_big;
	std::vector<std::multiset<big_number>> groups_of_msum, tmp_groups;
	{
		auto i1 = conjunctions.begin();
		auto i2 = i1;	i2++;
		if (i2 != conjunctions.end())
		{
			while (1)
			{
				{
					std::multiset<big_number> tmp_group;
					tmp_group.insert(*i1);
					tmp_group.insert(*i2);
					if (i1->crossing((*i2), tmp_big))
						tmp_group.insert(tmp_big);
					groups_of_msum.push_back(tmp_group);
				}
				i1 = i2;	i1++;
				if (i1 == conjunctions.end()) break;
				i2 = i1;	i2++;
				if (i2 == conjunctions.end())
				{
					std::multiset<big_number> tmp_group;
					tmp_group.insert(*i1);
					groups_of_msum.push_back(tmp_group);
					break;
				}
			}
		}
		else
		{
			std::multiset<big_number> tmp_group;
			tmp_group.insert(*i1);
			groups_of_msum.push_back(tmp_group);
		}
	}
	{
		//������ � ������ � ������
		while (groups_of_msum.size() > 1)
		{
			//printf("\nCount of groups %d\n", (int)groups_of_msum.size());
			auto i1 = groups_of_msum.begin();
			auto i2 = i1;
			i2++;
			tmp_groups.clear();
			while(1)
			{
				std::multiset<big_number> tmp_group;
				for (auto &ii2 : (*i2))
				{
					tmp_group.insert(ii2);
				}
				unsigned int count_it = 0;
				for (auto &ii1 : (*i1))
				{
					tmp_group.insert(ii1);
					for (auto &ii2 : (*i2))
					{
						if (ii1.crossing(ii2, tmp_big))
						{
							tmp_group.insert(tmp_big);
							count_it++;
						}
					}
					count_it++;
					if (count_it > 2000000)
					{
						count_it = 0;
						std::multiset<big_number> tmp_zh;
						auto zi1 = tmp_group.begin();
						auto zi2 = zi1;
						zi2++;
						if (zi2 != tmp_group.end())
						{
							while (1)
							{
								if ((*zi1) == (*zi2))
								{
									zi1 = zi2;
									zi1++;
									if (zi1 == tmp_group.end()) break;
									zi2 = zi1;
									zi2++;
									if (zi2 == tmp_group.end())
									{
										tmp_zh.insert(*zi1);
										break;
									}
									continue;
								}
								tmp_zh.insert(*zi1);
								zi1 = zi2;
								zi2++;
								if (zi2 == tmp_group.end())
								{
									tmp_zh.insert(*zi1);
									break;
								}
							}
							tmp_group = tmp_zh;
						}
						else
						{
							printf("\nError (to_zhrgalkin): 895\n");
						}
					}
				}
				//to exclude equals
				{
					std::multiset<big_number> tmp_zh;
					auto zi1 = tmp_group.begin();
					auto zi2 = zi1;
					zi2++;
					if (zi2 != tmp_group.end())
					{
						while (1)
						{
							if ((*zi1) == (*zi2))
							{
								zi1 = zi2;
								zi1++;
								if (zi1 == tmp_group.end()) break;
								zi2 = zi1;
								zi2++;
								if (zi2 == tmp_group.end())
								{
									tmp_zh.insert(*zi1);
									break;
								}
								continue;
							}
							tmp_zh.insert(*zi1);
							zi1 = zi2;
							zi2++;
							if (zi2 == tmp_group.end())
							{
								tmp_zh.insert(*zi1);
								break;
							}
						}
						tmp_groups.push_back(tmp_zh);
					}
					else
					{
						tmp_groups.push_back(tmp_group);
					}
				}
				i1++;
				i1++;
				if (i1 == groups_of_msum.end()) break;
				i2++;
				i2++;
				if (i2 == groups_of_msum.end())
				{
					tmp_groups.push_back(*i1);
					break;
				}
			}
			//to tmp_groups
			groups_of_msum = tmp_groups;
		}
		std::multiset<big_number> zhegalkin_polinom_v;
		//^x=x ^ 1
		auto i1 = groups_of_msum.begin();
		unsigned nsize = i1->begin()->get_n();
		for (unsigned ni = 0; ni <= nsize; ni++) //TODO: only if zeros??
		{
			bool is_changed = false;
			zhegalkin_polinom_v.clear();
			for (auto &ii : (*i1))
			{
				std::vector<big_number> tmp_vector = ii.get_zhegalkin_vector(ni);
				zhegalkin_polinom_v.insert(tmp_vector.begin(), tmp_vector.end());
				if (tmp_vector.size() > 1) is_changed = true;
			}
			if (!is_changed) continue;
			//Delete equals
			{
				std::multiset<big_number> tmp_zh;
				auto zi1 = zhegalkin_polinom_v.begin();
				auto zi2 = zi1;
				zi2++;
				if (zi2 != zhegalkin_polinom_v.end())
				{
					while (1)
					{
						if ((*zi1) == (*zi2))
						{
							zi1 = zi2;
							zi1++;
							if (zi1 == zhegalkin_polinom_v.end()) break;
							zi2 = zi1;
							zi2++;
							if (zi2 == zhegalkin_polinom_v.end())
							{
								tmp_zh.insert(*zi1);
								break;
							}
							continue;
						}
						tmp_zh.insert(*zi1);
						zi1 = zi2;
						zi2++;
						if (zi2 == zhegalkin_polinom_v.end())
						{
							tmp_zh.insert(*zi1);
							break;
						}
					}
					zhegalkin_polinom_v = tmp_zh;
				}
			}
			(*i1) = zhegalkin_polinom_v;
		}
		zhegalkin_polinom.insert(zhegalkin_polinom.begin(), zhegalkin_polinom_v.begin(), zhegalkin_polinom_v.end());
	}
}

unsigned int k_function::get_complexity()
{
	if (!zhegalkin || (type == 1))
	{
		return 1;
	}
	else // if(type == 2)
	{
		if (zhegalkin_polinom.size() == 0) return 0;
		unsigned int cnt = 0;
		//big_number common_and = *zhegalkin_polinom.begin();
		for (auto &i : zhegalkin_polinom)
		{
			cnt += i.get_number_of_code(0x2);
			//common_and = common_and & i;
		}
		return cnt;
		//return common_and.get_number_of_code(0x2);
	}
}

bool k_function::operator == (const k_function& target)
{
	if (zhegalkin) return (zhegalkin_polinom == target.zhegalkin_polinom);
	if (conjunctions == target.conjunctions) return true;
	for (auto i = conjunctions.begin(), j = target.conjunctions.begin(); (i != conjunctions.end()) || (j != target.conjunctions.end()); i++, j++)
	{
		if (i != conjunctions.end() && j != target.conjunctions.end())
			if ((*i) == (*j)) continue;
		bool there_is = false;
		k_function a, b;
		do {
			auto jj = j;
			if (i != conjunctions.end())
				for (; jj != target.conjunctions.end(); jj++)
				{
					if ((*i) == (*jj)) {
						there_is = true;
						break;
					}
				}
			else
				jj = target.conjunctions.end();
			if (!there_is && (i != conjunctions.end())) {
				if (a.conjunctions.size() != 0) return false;
				a.conjunctions.insert(*i);
				i++;
			}
			else {
				for (; jj != j; j++) {
					if (b.conjunctions.size() != 0) return false;
					b.conjunctions.insert(*j);
				}
				break;
			}
		} while (!there_is);
		if (a.conjunctions.size() > 1 || b.conjunctions.size() > 1) {
			return false;
		}
		//�������� this
		if (a.conjunctions.size() != 0) {
			for (auto &ai : a.conjunctions) {
				k_function tmp;
				big_number tmp_big;
				for (auto &ti : target.conjunctions) {
					if (ai.crossing(ti, tmp_big)) {
						tmp.conjunctions.insert(tmp_big);
					}
				}
				tmp.minimization();
				if (tmp.conjunctions.size() == 1) {
					if (!(ai == (*tmp.conjunctions.begin()))) return false;
				}
				else	return false;
			}
		}
		//�������� target
		if (b.conjunctions.size() != 0) {
			for (auto &bi : b.conjunctions) {
				k_function tmp;
				big_number tmp_big;
				for (auto &ti : conjunctions) {
					if (bi.crossing(ti, tmp_big)) {
						tmp.conjunctions.insert(tmp_big);
					}
				}
				tmp.minimization();
				if (tmp.conjunctions.size() == 1) {
					if (!(bi == (*tmp.conjunctions.begin()))) return false;
				}
				else		return false;
			}
		}
		if (a.conjunctions.size() == 0 && b.conjunctions.size() == 0) {
			printf("must not be here\n");
			return false;
		}
		if ((i == conjunctions.end()) && (j == target.conjunctions.end())) break;
	}
	return true;
}
bool k_function::operator == (const k_function& target) const
{
	if (zhegalkin) return (zhegalkin_polinom == target.zhegalkin_polinom);
	if (conjunctions == target.conjunctions) return true;
	for (auto i = conjunctions.begin(), j = target.conjunctions.begin(); (i != conjunctions.end()) || (j != target.conjunctions.end()); i++, j++)
	{
		if (i != conjunctions.end() && j != target.conjunctions.end())
			if ((*i) == (*j)) continue;
		bool there_is = false;
		k_function a, b;
		do		{
			auto jj = j;
			if (i != conjunctions.end())
				for (; jj != target.conjunctions.end(); jj++)
				{
					if ((*i) == (*jj))		{
						there_is = true;
						break;
					}
				}
			else
				jj = target.conjunctions.end();
			if (!there_is && (i != conjunctions.end()))			{
				if (a.conjunctions.size() != 0) return false;
				a.conjunctions.insert(*i);
				i++;
			}
			else		{
				for (; jj != j; j++)	{
					if (b.conjunctions.size() != 0) return false;
					b.conjunctions.insert(*j);
				}
				break;
			}
		} while (!there_is);
		if (a.conjunctions.size() > 1 || b.conjunctions.size() > 1)		{
			return false;
		}
		//�������� this
		if (a.conjunctions.size() != 0)		{
			for (auto &ai : a.conjunctions)		{
				k_function tmp;
				big_number tmp_big;
				for (auto &ti : target.conjunctions)	{
					if (ai.crossing(ti, tmp_big))	{
						tmp.conjunctions.insert(tmp_big);
					}
				}
				tmp.minimization();
				if (tmp.conjunctions.size() == 1)	{
					if (!(ai == (*tmp.conjunctions.begin()))) return false;
				}
				else	return false;
			}
		}
		//�������� target
		if (b.conjunctions.size() != 0)		{
			for (auto &bi : b.conjunctions)		{
				k_function tmp;
				big_number tmp_big;
				for (auto &ti : conjunctions)		{
					if (bi.crossing(ti, tmp_big))		{
						tmp.conjunctions.insert(tmp_big);
					}
				}
				tmp.minimization();
				if (tmp.conjunctions.size() == 1)		{
					if (!(bi == (*tmp.conjunctions.begin()))) return false;
				}
				else		return false;
			}
		}
		if (a.conjunctions.size() == 0 && b.conjunctions.size() == 0)		{
			printf("must not be here\n");
			return false;
		}
		if ((i == conjunctions.end()) && (j == target.conjunctions.end())) break;
	}
	return true;
}
bool k_function::operator < (const k_function& target) 
{
	if (!zhegalkin) {
		if (*this == target) return false;
		if (conjunctions.size() == target.conjunctions.size()) {
			for (auto c1 = conjunctions.begin(), c2 = target.conjunctions.begin(); c1 != conjunctions.end(); c1++, c2++) {
				if ((*c1) == (*c2)) continue;
				if ((*c1) < (*c2)) return true;
				else return false;
			}
			return false;
		}
		if (conjunctions.size() < target.conjunctions.size())	return true;
		return false;
	}
	else {
		auto last1 = zhegalkin_polinom.back();
		auto last2 = target.zhegalkin_polinom.back();
		bool first_is_one = last1.is_one();
		bool second_is_one = last2.is_one();
		if (first_is_one == second_is_one) {
			if (zhegalkin_polinom.size() == target.zhegalkin_polinom.size()) {
				auto c2 = target.zhegalkin_polinom.begin();
				for (auto c1 = zhegalkin_polinom.begin(); c1 != zhegalkin_polinom.end(); c1++, c2++) {
					if ((*c1) == (*c2)) continue;
					if ((*c1) < (*c2)) return true;
					else return false;
				}
				return false;
			}
			if (zhegalkin_polinom.size() < target.zhegalkin_polinom.size())			return true;
			return false;
		}
		else {
			k_function tmp;
			if (first_is_one) {
				tmp = target;
				tmp.zhegalkin_polinom.push_back(last1);
				return (*this < tmp);
			}
			tmp = *this;
			tmp.zhegalkin_polinom.push_back(last2);
			return (tmp < target);
		}
	}
}
bool k_function::operator < (const k_function& target) const
{
	if (!zhegalkin)	{
		if (*this == target) return false;
		if (conjunctions.size() == target.conjunctions.size())	{
			for (auto c1 = conjunctions.begin(), c2 = target.conjunctions.begin(); c1 != conjunctions.end(); c1++, c2++)	{
				if ((*c1) == (*c2)) continue;
				if ((*c1) < (*c2)) return true;
				else return false;
			}
			return false;
		}
		if (conjunctions.size() < target.conjunctions.size())	return true;
		return false;
	}
	else	{
		auto last1 = zhegalkin_polinom.back();
		auto last2 = target.zhegalkin_polinom.back();
		bool first_is_one = last1.is_one();
		bool second_is_one = last2.is_one();
		if (first_is_one == second_is_one)	{
			if (zhegalkin_polinom.size() == target.zhegalkin_polinom.size())	{
				for (auto c1 = zhegalkin_polinom.begin(), c2 = target.zhegalkin_polinom.begin(); c1 != zhegalkin_polinom.end(); c1++, c2++)	{
					if ((*c1) == (*c2)) continue;
					if ((*c1) < (*c2)) return true;
					else return false;
				}
				return false;
			}
			if (zhegalkin_polinom.size() < target.zhegalkin_polinom.size())			return true;
			return false;
		}
		else {
			k_function tmp;
			if (first_is_one)		{
				tmp = target;
				tmp.zhegalkin_polinom.push_back(last1);
				return (*this < tmp);
			}
			tmp = *this;
			tmp.zhegalkin_polinom.push_back(last2);
			return (tmp < target);
		}
	}
}