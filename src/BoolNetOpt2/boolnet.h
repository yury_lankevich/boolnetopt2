#pragma once
#include <stdlib.h>
#include <string>
#include <set>
#include <vector>
#include "leaf.h"
#include "DNF_node.h"
#include <map>
class boolnet
{
public:
	boolnet();
	~boolnet();
	void add_system_root(base_node*);
	void add_system_leaf(base_node*);
	void add_internal_node(base_node*);
	void reduce(int mode = 1);
	unsigned int get_nodes_cnt();
	std::set<std::string> get_merge_result();
	std::set<std::string> get_all_roots();
	std::vector<std::string> convert2string(bool c = true);
	int get_next_color();
	void clean_up();

	boolnet* get_copy();
	void shannon_expansion(ID_TYPE id);

	struct comp_long_long {
		bool operator()(std::pair<std::string, ID_TYPE> a, std::pair<std::string, ID_TYPE> b)
		{
			return a.second < b.second;
		}
	};

	static std::map<std::string, ID_TYPE> *names_map;
	static std::map<ID_TYPE, std::string> *ids_map;
	static std::string get_name_by_id(ID_TYPE id);
	static unsigned char type;

	ID_TYPE get_id(std::string name);
	void print(std::string);
	void clean_merge_result();

	void make_DNF_nodes(); // Make some nodes DNF
protected:
	int next_color = 111;
	std::vector<base_node*> system_roots;
	std::set<base_node*> system_leafs;
	std::vector<base_node*> all_system_nodes;
	std::set<std::string> merge_result;

	bool replace_if_equal(base_node*& k, base_node*& j); // returns true if k becomes OTPUT & TRANSLATED, we don't need ti compare it anymore
	void additional_reduction();

	//typedef 
	struct group_of_nodes
	{
		ID_TYPE key;
		std::vector<base_node*> group;
	} ;
};