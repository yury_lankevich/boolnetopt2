#include "leaf.h"

base_node* leaf::get_instance_copy(ID_TYPE func_id_)
{
	return new leaf(func_id_, op, status);
}

void leaf::get_cunj(std::set<big_number>& bn_set, const big_number& conj, unsigned int pos, unsigned int left_pos, unsigned int right_pos)
{
	big_number bn = conj;
	char f = conj.get_code(pos);
	char left_val = left_pos <= conj.get_n() ? conj.get_code(left_pos) : 0;
	char right_val = right_pos <= conj.get_n() ? conj.get_code(right_pos) : 0;
	unsigned int n_left_pos; // => move to get DNF
	unsigned int n_right_pos;
	if (!left_val && !right_val)
	{
		unsigned int n = bn.get_n() + 1;
		bn.resize(n);
		n_left_pos = n;
		n_right_pos = pos;
	}
	else if (left_val && right_val)
	{
		bn.exclude_pos(pos);
		n_left_pos = left_pos > pos ? left_pos - 1 : left_pos;
		n_right_pos = right_pos > pos ? right_pos - 1 : right_pos;
	}
	else
	{
		n_left_pos = left_val == 0 ? pos : left_pos;
		n_right_pos = right_val == 0 ? pos : right_pos;
	}
	if (f == 1) // 0 => Inversion (DeMorgan)
	{
		if (op == '+')
		{
			bn.set_code(n_left_pos, inv[LEFT] ? 2 : 1);
			bn.set_code(n_right_pos, inv[RIGHT] ? 2 : 1);
			if ((left_val != (inv[LEFT] ? 1 : 2)) &&
				(right_val != (inv[RIGHT] ? 1 : 2)))
			{
				bn_set.insert(bn);
			}
		}
		else if (op == '*')
		{
			bn.set_code(n_left_pos, inv[LEFT] ? 2 : 1);
			bn.set_code(n_right_pos, 3);
			if (left_val != (inv[LEFT] ? 1 : 2))
			{
				bn_set.insert(bn);
			}
			bn.set_code(n_left_pos, 3);
			bn.set_code(n_right_pos, inv[RIGHT] ? 2 : 1);
			if (right_val != (inv[LEFT] ? 1 : 2))
			{
				bn_set.insert(bn);
			}
		}
		else
		{
			printf("\n[Error:leaf] upexpected op 2 %c\n", op);
		}
	}
	else if (f == 2) // 1
	{
		if (op == '+')
		{
			bn.set_code(n_left_pos, inv[LEFT] ? 1 : 2);
			bn.set_code(n_right_pos, 3);
			if (left_val != (inv[LEFT] ? 2 : 1))
			{
				bn_set.insert(bn);
			}
			bn.set_code(n_left_pos, 3);
			bn.set_code(n_right_pos, inv[RIGHT] ? 1 : 2);
			if (right_val != (inv[LEFT] ? 2 : 1))
			{
				bn_set.insert(bn);
			}
		}
		else if (op == '*')
		{
			bn.set_code(n_left_pos, inv[LEFT] ? 1 : 2);
			bn.set_code(n_right_pos, inv[RIGHT] ? 1 : 2);
			if ((left_val != (inv[LEFT] ? 2 : 1)) &&
				(right_val != (inv[RIGHT] ? 2 : 1)))
			{
				bn_set.insert(bn);
			}
		}
		else
		{
			printf("\n[Error:leaf] upexpected op 2 %c\n", op);
		}
	}
	else if (f == 3) // - Let's add expanded conjunktion
	{
		if(left_val == 0) bn.set_code(n_left_pos, 3);
		if(right_val == 0) bn.set_code(n_right_pos, 3);
		bn_set.insert(bn);
	}
	else // ? ERROR
	{
		printf("\n[Error:leaf] upexpected val of func\n");
	}
}

leaf::leaf(ID_TYPE func_id_, unsigned char status_) : base_node(func_id_, status_)
{
	op = '=';
}

leaf::leaf(ID_TYPE func_id_, char op_type_, unsigned char status_) : base_node(func_id_, status_)
{
	op = op_type_;

	if (!(status & INPUT)) {
		if (op == '=' || op == '^')
		{
			status = (leaf_type_e)(status | TRANSLATED);
			if (func_id_ == ZERO_ID)
			{
				status = (leaf_type_e)(status | ZERO);
			}
			else if (func_id_ == ONE_ID)
			{
				status = (leaf_type_e)(status | ONE);
			}
			else
			{
				linked_down_array_size = 1;
				items_beneath_cnt = 1;
				linked_down_array = new base_node * [1];
				inv = new bool[1];
				inv[LEFT] = (op == '^');
			}
			op = '=';
		}
	}
}

ID_TYPE leaf::get_key()
{
	ID_TYPE xor_func = 0;
	if (get_leaf_type() & TRANSLATED)
	{
		if (inv[LEFT])
		{
			return ((get_id_0() << 8) & FUNC2_TYPE_MASK) ^ FUNC2_TYPE2;
		}
		else
		{
			return ((get_id_0() << 8) & FUNC2_TYPE_MASK) ^ FUNC2_TYPE3;
		}
	}
	if (get_id_0() > get_id_1())
	{
		if (op == '*')
		{
			if (inv[RIGHT] && inv[LEFT])
			{
				xor_func = FUNC2_TYPE1;
			}
			else if (inv[RIGHT] && !inv[LEFT])
			{
				xor_func = FUNC2_TYPE2;
			}
			else if (!inv[RIGHT] && inv[LEFT])
			{
				xor_func = FUNC2_TYPE3;
			}
			else
			{
				xor_func = FUNC2_TYPE4;
			}
		}
		else if (op == '+')
		{
			if (inv[RIGHT] && inv[LEFT])
			{
				xor_func = FUNC2_TYPE4;
			}
			else if (inv[RIGHT] && !inv[LEFT])
			{
				xor_func = FUNC2_TYPE3;
			}
			else if (!inv[RIGHT] && inv[LEFT])
			{
				xor_func = FUNC2_TYPE2;
			}
			else
			{
				xor_func = FUNC2_TYPE1;
			}
		}
		else if (op == '@' || op == '%')
		{
			xor_func = FUNC2_TYPE5;
		}
		return (( ( (get_id_0() + get_id_1()) ) ^ (get_id_0() << 7) ) & FUNC2_TYPE_MASK) ^ (xor_func & ~FUNC2_TYPE_MASK);// ^ (op + left_inv + right_inv); //equal_hash;
	}
	else
	{
		if (op == '*')
		{
			if (inv[RIGHT] && inv[LEFT])
			{
				xor_func = FUNC2_TYPE1;
			}
			else if (!inv[RIGHT] && inv[LEFT])
			{
				xor_func = FUNC2_TYPE2;
			}
			else if (inv[RIGHT] && !inv[LEFT])
			{
				xor_func = FUNC2_TYPE3;
			}
			else
			{
				xor_func = FUNC2_TYPE4;
			}
		}
		else if (op == '+')
		{
			if (inv[RIGHT] && inv[LEFT])
			{
				xor_func = FUNC2_TYPE4;
			}
			else if (!inv[RIGHT] && inv[LEFT])
			{
				xor_func = FUNC2_TYPE3;
			}
			else if (inv[RIGHT] && !inv[LEFT])
			{
				xor_func = FUNC2_TYPE2;
			}
			else
			{
				xor_func = FUNC2_TYPE1;
			}
		}
		else if (op == '@' || op == '%')
		{
			xor_func = FUNC2_TYPE5;
		}
		return (( ( (get_id_0() + get_id_1()) ) ^ (get_id_1() << 7) ) & FUNC2_TYPE_MASK) ^ (xor_func & ~FUNC2_TYPE_MASK);// ^ (op + left_inv + right_inv); //equal_hash;
	}
}

void leaf::update()
{
	if (lock_update || (get_leaf_type() & (EXCLUDED | ZERO | ONE | INPUT)) )
		return;
	if (op != '*' && op != '+' && op != '=' && op != '@' && op != '%') // @ -> a == b ... % -> a xor b
	{
		printf("Error: leaf update\n");
		exit(1);
	}
	char res_code = 0x00;
	
	if (get_right() != nullptr)
	{
		if (inv[RIGHT] == true)
		{
			if (get_right()->get_leaf_type() & ZERO)
			{
				res_code |= 0x03;
			} else if(!(get_right()->get_leaf_type() & ONE))
			{
				res_code |= 0x01;
			}
		}
		else
		{
			if (get_right()->get_leaf_type() & ONE)
			{
				res_code |= 0x03;
			}
			else if (!(get_right()->get_leaf_type() & ZERO))
			{
				res_code |= 0x01;
			}
		}
	}
	if (get_left() != nullptr)
	{
		if (inv[LEFT] == true)
		{
			if (get_left()->get_leaf_type() & ZERO)
			{
				res_code |= 0x0c;
			}
			else if (!(get_left()->get_leaf_type() & ONE))
			{
				res_code |= 0x04;
			}
		}
		else
		{
			if (get_left()->get_leaf_type() & ONE)
			{
				res_code |= 0x0c;
			}
			else if (!(get_left()->get_leaf_type() & ZERO))
			{
				res_code |= 0x04;
			}
		}
	}
	switch (res_code)
	{
		case 0x03: // 0 op 1
			if (op == '=')
			{
				printf("Error: leaf update... cannot be res code 0xc for op '='\n");
			}
			[[fallthrough]];
		case 0x0c: // 1 op 0
		{
			if(op == '*' || op == '@')
			{
				status = (leaf_type_e)((status & OUTPUT) | ZERO );
			}
			else
			{
				status = (leaf_type_e)((status & OUTPUT) | ONE );
			}
			break;
		}
		case 0x00: // 0 op 0
		{
			if (op == '@')
			{
				status = (leaf_type_e)((status & OUTPUT) | ONE);
			}
			else
			{
				status = (leaf_type_e)((status & OUTPUT) | ZERO);
			}
			break;
		}
		case 0x0f: // 1 op 1
		{
			if (op == '%')
			{
				status = (leaf_type_e)((status & OUTPUT) | ZERO);
			}
			else
			{
				status = (leaf_type_e)((status & OUTPUT) | ONE);
			}
			break;
		}
		case 0x01: // 0 op right
		{
			if (op == '*')
			{
				status = (leaf_type_e)((status & OUTPUT) | ZERO );
			}
			else
			{
				if (get_left() != nullptr)
				{
					get_left()->delete_linked_up(this);
				}
				linked_down_array[0] = get_right();
				items_beneath_cnt = 1;
				status = (leaf_type_e)((status & (OUTPUT )) | TRANSLATED);
				if (op == '@') // op is "=="
				{
					inv[LEFT] = !inv[RIGHT];
				}
				else
				{
					inv[LEFT] = inv[RIGHT];
				}
			}
			break;
		}
		case 0x04: // left op 0
		{
			if (op == '*' || op == '=') // when op is '=' we have TRANSLATED case
			{
				if (get_right() != nullptr)
				{
					status = (leaf_type_e)((status & OUTPUT) | ZERO);
				}
			}
			else
			{
				if (op == '@') // op is "=="
				{
					inv[LEFT] = !inv[LEFT];
				}

				if (get_right() != nullptr)
				{
					get_right()->delete_linked_up(this);
				}
				status = (leaf_type_e)((status & (OUTPUT )) | TRANSLATED);
			}
			break;
		}
		case 0x05: // left op right
		{
			bool they_are_equal = false;
			bool their_inv_are_equal = false;
			if ((get_left()->get_leaf_type() ) == (get_right()->get_leaf_type() ))
			{
				if (get_left()->get_leaf_type() & INPUT)
				{
					if (get_left()->get_func_id() == get_right()->get_func_id()) {
						they_are_equal = true;
					}
				}
				else
				{
					if (get_left()->is_it_equal(get_right()))
					{
						they_are_equal = true;
					}
					/*else if (get_left()->is_inv_equal(get_right()))
					{
						their_inv_are_equal = true;
					}*/
				}
			}
			if (they_are_equal || their_inv_are_equal)
			{
				if ((they_are_equal && (inv[RIGHT] != inv[LEFT])) || (their_inv_are_equal && (inv[RIGHT] == inv[LEFT])))
				{
					if (op == '*' || op == '@')
					{
						status = (leaf_type_e)((status & OUTPUT) | ZERO );
					}
					else // op == '+'
					{
						status = (leaf_type_e)((status & OUTPUT) | ONE );
					}
				}
				else
				{
					status = (leaf_type_e)((status & (OUTPUT )) | TRANSLATED);
					if (get_left() != get_right())
					{
						get_right()->delete_linked_up(this);
					}
					set_right(nullptr);
				}
				//op = '=';
			}
			else if (op == '@' || op == '%')
			{
				if (inv[LEFT] != inv[RIGHT])
				{
					op = (op == '@') ? '%' : '@';
				}
				inv[LEFT] = false;
				inv[RIGHT] = false;
			}
			else
			{
				leaf *left, *right;
				left = dynamic_cast<leaf*>(get_left());
				right = dynamic_cast<leaf*>(get_right());
				if (left != nullptr && right != nullptr && 
					right->get_right() != nullptr && right->get_left() != nullptr && left->get_right() != nullptr && left->get_left() != nullptr && !without_xor)
				{
					ID_TYPE r_id0 = right->get_id_0();
					ID_TYPE r_id1 = right->get_id_1();
					ID_TYPE l_id0 = left->get_id_0();
					ID_TYPE l_id1 = left->get_id_1();
					ID_TYPE r_id_min, r_id_max, l_id_min, l_id_max;
					bool r_inv_order = r_id0 > r_id1;
					bool l_inv_order = l_id0 > l_id1;
					bool r_min_inv, r_max_inv, l_min_inv, l_max_inv;
					if (r_inv_order)
					{
						r_id_min = r_id1;
						r_min_inv = get_right()->inv[RIGHT];
						r_id_max = r_id0;
						r_max_inv = get_right()->inv[LEFT];
					}
					else
					{
						r_id_min = r_id0;
						r_min_inv = get_right()->inv[LEFT];
						r_id_max = r_id1;
						r_max_inv = get_right()->inv[RIGHT];
					}
					if (l_inv_order)
					{
						l_id_min = l_id1;
						l_min_inv = get_left()->inv[RIGHT];
						l_id_max = l_id0;
						l_max_inv = get_left()->inv[LEFT];
					}
					else
					{
						l_id_min = l_id0;
						l_min_inv = get_left()->inv[LEFT];
						l_id_max = l_id1;
						l_max_inv = get_left()->inv[RIGHT];
					}
					if (r_id_min == l_id_min && r_id_max == l_id_max)
					{
						if (op == '+') // f1 + f2
						{
							if (((!inv[LEFT] && left->get_op() == '*') || (inv[LEFT] && left->get_op() == '+')) &&
								((!inv[RIGHT] && right->get_op() == '*') || (inv[RIGHT] && right->get_op() == '+'))) // a1*a2+a1*a2
							{
								if (r_min_inv == r_max_inv && l_min_inv == l_max_inv)
								{
									if ( (r_min_inv ^ inv[RIGHT]) != (l_min_inv ^ inv[LEFT]))
									{
										op = '@'; // a == b
									}
									else
									{
										printf("leaf:640: Error\n");
									}
								}
								else if (r_min_inv != r_max_inv && l_min_inv != l_max_inv)
								{
									if ((r_min_inv ^ inv[RIGHT]) != (l_min_inv ^ inv[LEFT]))
									{
										op = '%'; // a xor b
									}
									else
									{
										printf("leaf:652: Error\n");
									}
								}
								else
								{
									get_left()->delete_linked_up(this);
									get_right()->delete_linked_up(this);
									status = (leaf_type_e)((status & (OUTPUT)) | TRANSLATED);
									if ((r_min_inv ^ inv[RIGHT]) == (l_min_inv ^ inv[LEFT]))
									{
										set_left(l_inv_order ? left->get_right() : left->get_left());
										inv[LEFT] = l_min_inv ^ inv[LEFT];
									}
									else
									{
										set_left(l_inv_order ? left->get_left() : left->get_right());
										inv[LEFT] = l_max_inv ^ inv[LEFT];
									}
									//get_left()->add_linked_up(this);
								}
							}
							else
							{
								//TODO
							}
						}
						else //if(op == '*')// f1*f2
						{
							if (((!inv[LEFT] && left->get_op() == '+') || (inv[LEFT] && left->get_op() == '*')) &&
								((!inv[RIGHT] && right->get_op() == '+') || (inv[RIGHT] && right->get_op() == '*')))
							{
								if (r_min_inv == r_max_inv && l_min_inv == l_max_inv)
								{
									if ((r_min_inv ^ inv[RIGHT]) != (l_min_inv ^ inv[LEFT]))
									{
										op = '%'; // a xor b
									}
									else
									{
										printf("leaf:709: Error\n");
									}
								}
								else if (r_min_inv != r_max_inv && l_min_inv != l_max_inv)
								{
									if ((r_min_inv ^ inv[RIGHT]) != (l_min_inv ^ inv[LEFT]))
									{
										op = '@'; // a == b
									}
									else
									{
										printf("leaf:726: Error\n");
									}
								}
								else
								{
									get_left()->delete_linked_up(this);
									get_right()->delete_linked_up(this);
									status = (leaf_type_e)((status & (OUTPUT)) | TRANSLATED);
									op = '=';
									if ((r_min_inv ^ inv[RIGHT]) == (l_min_inv ^ inv[LEFT]))
									{
										set_left(l_inv_order ? left->get_right() : left->get_left());
										inv[LEFT] = l_min_inv ^ inv[LEFT];
									}
									else
									{
										set_left(l_inv_order ? left->get_left() : left->get_right());
										inv[LEFT] = l_max_inv ^ inv[LEFT];
									}
									//get_left()->add_linked_up(this);
								}
							}
							else
							{
								//TODO
							}
						}
					}
					if (op == '%' || op == '@')
					{
						get_left()->delete_linked_up(this);
						get_right()->delete_linked_up(this);
						set_right(left->get_right());
						set_left(left->get_left());
						//get_left()->add_linked_up(this); We did this in set left!
						//get_right()->add_linked_up(this); We did this in set right!
						inv[LEFT] = false;
						inv[RIGHT] = false;
					}
				}
			}
			break;
		}
		case 0x07: // left op 1
		{
			if (op == '+' || op == '=')
			{
				if (get_right() != nullptr)
				{
					status = (leaf_type_e)((status & OUTPUT) | ONE );
				}
				else
				{
					//TODO:
					printf("What is it ? line 743\n");
				}
			}
			else
			{
				status = (leaf_type_e)((status & (OUTPUT )) | TRANSLATED);
				if (get_right() != nullptr)
				{
					get_right()->delete_linked_up(this);
				}

				if (op == '%') //TODO: op == @
				{
					inv[LEFT] = !inv[LEFT];
				}
			}
			break;
		}
		case 0x0d: // 1 op right
		{
			if (op == '+')
			{
				status = (leaf_type_e)((status & OUTPUT) | ONE );
			}
			else
			{
				status = (leaf_type_e)((status & (OUTPUT )) | TRANSLATED);
				if (get_left() != nullptr)
				{
					get_left()->delete_linked_up(this);
				}
				linked_down_array[0] = get_right();
				items_beneath_cnt = 1;
				if (op == '%') // TODO: op == @
				{
					inv[LEFT] = !inv[RIGHT];
				}
				else
				{
					inv[LEFT] = inv[RIGHT];
				}
			}
			break;
		}
	}
	if (get_leaf_type() & (ONE | ZERO))
	{
		op = '=';
		if (get_right() != nullptr)
		{
			get_right()->delete_linked_up(this);
			set_right(nullptr);
		}
		if (get_left() != nullptr)
		{
			get_left()->delete_linked_up(this);
			set_left(nullptr);
		}
		items_beneath_cnt = 0;
	}
	else if(get_leaf_type() & TRANSLATED)
	{
		op = '=';
		set_right(nullptr);
	}

	if( (get_leaf_type() & TRANSLATED) && ! is_output())
	{
		status = EXCLUDED;
		replace(get_left(), inv[LEFT]);
	}
	if (level) // only in case if we didn't use update level!!!
	{
		unsigned level_right = 0, level_left = 0;
		if (get_right() != nullptr)
		{
			level_right = get_right()->level;// update_level(color);
		}
		if (get_left() != nullptr)
		{
			level_left = get_left()->level;// update_level(color);
		}
		level = level_left > level_right ? level_left : level_right;
		++level;
	}
}

void leaf::set_translate(base_node* to, bool inv)
{
	base_node::set_translate(to, inv);
	op = '=';
}

void leaf::set_left(base_node* left_)
{
	if (linked_down_array_size < 1)
	{
		linked_down_array_size = 2;
		items_beneath_cnt = 1;
		linked_down_array = new base_node * [2];
		inv = new bool[2];
		inv[LEFT] = false;
		inv[RIGHT] = false;
	}
	else if (items_beneath_cnt > 2)
	{
		printf("[Error: set_left()]: unexpected usage, looks like set_left() has been called for DNF table node\n");
	}
	else if (items_beneath_cnt == 0)
	{
		items_beneath_cnt = 1;
	}
	linked_down_array[0] = left_;
	if (left_ == nullptr)
	{
		return;
	}
	if (get_left() != get_right())
	{
		get_left()->add_linked_up(this);
	}
	else
	{
		if (inv[RIGHT] != inv[LEFT])
		{			
			if (op == '+' || op == '%')
			{
				op = '=';
				status = (leaf_type_e)((status & OUTPUT) | ONE );
				inv[RIGHT] = false;
				inv[LEFT] = false;
			}
			else if (op == '*' || op == '@')
			{
				op = '=';
				status = (leaf_type_e)((status & OUTPUT) | ZERO );
				inv[RIGHT] = false;
				inv[LEFT] = false;
			}
			else
			{
				printf("Error: unexpected operator\n");
			}
			get_left()->delete_linked_up(this);
			items_beneath_cnt = 0;
		}
		else
		{
			if (op == '@')
			{
				op = '=';
				status = (leaf_type_e)((status & OUTPUT) | ONE);
				inv[RIGHT] = false;
				inv[LEFT] = false;
			}
			else if (op == '%')
			{
				op = '=';
				status = (leaf_type_e)((status & OUTPUT) | ZERO);
				inv[RIGHT] = false;
				inv[LEFT] = false;
			}
			else
			{
				items_beneath_cnt = 1;
				inv[RIGHT] = false;
				status = (leaf_type_e)((status & (OUTPUT)) | TRANSLATED);
				op = '=';
			}
		}
	}
}
void leaf::set_right(base_node* right_)
{
	if (items_beneath_cnt < 1 || linked_down_array_size < 2)
	{
		if (right_ == nullptr) return;
		printf("[Error: set_right()]: pls set left first\n");
	}
	else if(items_beneath_cnt > 2)
	{
		printf("[Error: set_right()]: unexpected usage, looks like set_right() has been called for DNF table node\n");
	}
	else
	{
		items_beneath_cnt = 2;
	}
	linked_down_array[1] = right_;
	if (right_ == nullptr)
	{
		if (get_left() == nullptr)
			items_beneath_cnt = 0;
		else
			items_beneath_cnt = 1;
		return;
	}
	if (get_left() != get_right())
	{
		get_right()->add_linked_up(this);
	}
	else
	{
		if (inv[RIGHT] != inv[LEFT])
		{
			if (op == '+' || op == '%')
			{
				op = '=';
				status = (leaf_type_e)((status & OUTPUT) | ONE );
				inv[RIGHT] = false;
				inv[LEFT] = false;
			}
			else if (op == '*' || op == '@')
			{
				op = '=';
				status = (leaf_type_e)((status & OUTPUT) | ZERO );
				inv[RIGHT] = false;
				inv[LEFT] = false;
			}
			else
			{
				printf("Error: unexpected operator\n");
			}
			get_right()->delete_linked_up(this);
			items_beneath_cnt = 0;
		}
		else
		{
			if (op == '@')
			{
				op = '=';
				status = (leaf_type_e)((status & OUTPUT) | ONE);
				inv[RIGHT] = false;
				inv[LEFT] = false;
			}
			else if (op == '%')
			{
				op = '=';
				status = (leaf_type_e)((status & OUTPUT) | ZERO);
				inv[RIGHT] = false;
				inv[LEFT] = false;
			}
			else
			{
				items_beneath_cnt = 1;
				inv[RIGHT] = false;
				status = (leaf_type_e)((status & (OUTPUT)) | TRANSLATED);
				op = '=';
			}
		}
	}
}

void leaf::get_DNF(std::set<big_number>& dnf_set, std::map<base_node*, int>& leaf_map, int color_)
{
	if (this->get_leaf_type() & (INPUT | ONE | ZERO) || color_ != color) return;
	else if (this->get_leaf_type() == TRANSLATED) {
		printf("[Error] Wrong usage of get_DNF function (TRANSLATED leaf)");
		get_left()->get_DNF(dnf_set, leaf_map, color_);
	}
	else if (leaf_map.size() == 0)
	{
		if (get_left() != nullptr && get_right() != nullptr)
		{
			std::string str;
			leaf_map.insert({ get_left() , 0 });
			leaf_map.insert({ get_right(), 1 });
			if (op == '*') {
				str = std::string(inv[LEFT] ? "0" : "1") + (inv[RIGHT] ? "0" : "1");
				dnf_set.insert(big_number(str));
			}
			else if (op == '+') {
				str = std::string(inv[LEFT] ? "0-" : "1-");
				dnf_set.insert(big_number(str));
				str = std::string(inv[RIGHT] ? "-0" : "-1");
				dnf_set.insert(big_number(str));
			}
			/*else if (op == '%')
			{

			}
			else if (op == '@')
			{

			}*/
			else
			{
				printf("[Error:leaf] upexpected op %c", op);
			}
		}
		else
		{
			printf("[Error] Wrong usage of get_DNF function (left or right is null)");
		}
	}
	else
	{
		auto ptr = leaf_map.find(this);
		if (ptr == leaf_map.end())
		{
			printf("[Error] Wrong usage of get_DNF function (node is not found in leaf_map)");
		}
		else
		{
			auto ptr_left = leaf_map.find(get_left());
			auto ptr_right = leaf_map.find(get_right());
			unsigned int inc_n = dnf_set.begin()->get_n() + 1;
			unsigned int left_pos = (ptr_left == leaf_map.end()) ? inc_n : ptr_left->second;
			unsigned int right_pos = (ptr_right == leaf_map.end()) ? inc_n : ptr_right->second;
			int pos = ptr->second;
			leaf_map.erase(ptr);

			unsigned int n_left_pos;
			unsigned int n_right_pos;
			if (left_pos == inc_n && right_pos == inc_n) // (left_pos == right_pos)
			{
				n_left_pos = inc_n;
				n_right_pos = pos;
			}
			else if (left_pos != inc_n && right_pos != inc_n)
			{
				n_left_pos = left_pos > pos ? left_pos - 1 : left_pos;
				n_right_pos = right_pos > pos ? right_pos - 1 : right_pos;
				for (auto& i : leaf_map)
				{
					if (i.second > pos)
						i.second -= 1;
				}
			}
			else
			{
				n_left_pos = left_pos == inc_n ? pos : left_pos;
				n_right_pos = right_pos == inc_n ? pos : right_pos;
			}

			leaf_map.insert({ get_left() , n_left_pos });
			leaf_map.insert({ get_right() , n_right_pos });

			std::set<big_number> dnf_set_tmp;
			std::string str;
			for (auto& i : dnf_set)
			{
				get_cunj(dnf_set_tmp, i, pos, left_pos, right_pos);
			}
			// �������� ���� � ID = ptr->second DNF ���������� �� ������ ������
			// ��������� ���� �� � leaf_map left or right
			// ������ ��� ��� ���������� dnf_set
			dnf_set = dnf_set_tmp;
		}
	}
	for (unsigned int i = 0; i < items_beneath_cnt; i++)
	{
		linked_down_array[i]->get_DNF(dnf_set, leaf_map, color_);
	}
}

std::string leaf::get_result_function(std::string (*name_by_id) (ID_TYPE id))
{
	std::string func_name_res = is_output() ? name_by_id(func_id) : "TN_" + std::to_string(func_id);
	std::string func_name_0;
	std::string func_name_1;
	bool without_inv = (op == '@' || op == '%');
	if (get_left() != nullptr)
	{
		if (get_left()->get_leaf_type() & ZERO)
		{
			func_name_0 = inv[LEFT] && !without_inv ? "1" : "0";
		}
		else if (get_left()->get_leaf_type() & ONE)
		{
			func_name_0 = inv[LEFT] && !without_inv ? "0" : "1";
		}
		else
		{
			func_name_0 = (inv[LEFT] && !without_inv) ? "^" + name_by_id(get_left()->get_func_id()) : name_by_id(get_left()->get_func_id());
		}
	}
	else
	{
		func_name_0 = (get_id_0() == ZERO_ID) ? (inv[LEFT] && !without_inv ? "1" : "0") : (inv[LEFT] && !without_inv ? "0" : "1");
	}
	if (get_right() != nullptr)
	{
		if (get_right()->get_leaf_type() & ZERO)
		{
			func_name_1 = inv[RIGHT] && !without_inv ? "1" : "0";
		}
		else if (get_right()->get_leaf_type() & ONE)
		{
			func_name_1 = inv[RIGHT] && !without_inv ? "0" : "1";
		}
		else
		{
			func_name_1 = inv[RIGHT] && !without_inv ? "^" + name_by_id(get_right()->get_func_id()) : name_by_id(get_right()->get_func_id());
		}
	}
	else
	{
		func_name_1 = (get_id_1() == ZERO_ID) ? (inv[RIGHT] && !without_inv ? "1" : "0") : (inv[RIGHT] && !without_inv ? "0" : "1");
	}
	if (op == '+' || op == '*')
	{
		if (get_leaf_type() & TRANSLATED)
		{
			printf("\n[Error] [leaf::get_result_function] op = '*' or '+' and type TRANSLATED should not be together\n");
		}
		return func_name_res + '=' + func_name_0 + op + func_name_1;
	}
	else if (op == '=')
	{
		return func_name_res + '=' + func_name_0;
	}
	else if(op == '^')
	{
		return func_name_res + '=' + '^' + func_name_0;
	}
	else if (op == '%')
	{
		if (inv[LEFT] == inv[RIGHT])
		{
			return func_name_res + '=' + '^' + func_name_0 + '*' + func_name_1 + '+' + func_name_0 + '*' + '^' + func_name_1;
		}
		else
		{
			return func_name_res + '=' + func_name_0 + '*' + func_name_1 + '+' + '^' + func_name_0 + '*' + '^' + func_name_1;
		}
	}
	else if (op == '@')
	{
		if (inv[LEFT] != inv[RIGHT])
		{
			return func_name_res + '=' + '^' + func_name_0 + '*' + func_name_1 + '+' + func_name_0 + '*' + '^' + func_name_1;
		}
		else
		{
			return func_name_res + '=' + func_name_0 + '*' + func_name_1 + '+' + '^' + func_name_0 + '*' + '^' + func_name_1;
		}
	}
	printf("leaf.cpp:1167 :: Error case\n");
	return "Error case";//
}

std::string leaf::get_equal(std::string(*name_by_id) (ID_TYPE id))
{
	std::string func_name_res = is_output() ? name_by_id(func_id) : "TN_" + std::to_string(func_id);
	std::string func_name_0;
	std::string func_name_1;
	bool without_inv = (op == '@' || op == '%');
	if (get_left() != nullptr)
	{
		if (get_left()->get_leaf_type() & ZERO)
		{
			func_name_0 = inv[LEFT] && !without_inv ? "1" : "0";
		}
		else if (get_left()->get_leaf_type() & ONE)
		{
			func_name_0 = inv[LEFT] && !without_inv ? "0" : "1";
		}
		else
		{
			func_name_0 = (inv[LEFT] && !without_inv) ? "^" + name_by_id(get_left()->get_func_id()) : name_by_id(get_left()->get_func_id());
		}
	}
	else
	{
		func_name_0 = (get_id_0() == ZERO_ID) ? (inv[LEFT] && !without_inv ? "1" : "0") : (inv[LEFT] && !without_inv ? "0" : "1");
	}
	if (get_right() != nullptr)
	{
		if (get_right()->get_leaf_type() & ZERO)
		{
			func_name_1 = inv[RIGHT] && !without_inv ? "1" : "0";
		}
		else if (get_right()->get_leaf_type() & ONE)
		{
			func_name_1 = inv[RIGHT] && !without_inv ? "0" : "1";
		}
		else
		{
			func_name_1 = inv[RIGHT] && !without_inv ? "^" + name_by_id(get_right()->get_func_id()) : name_by_id(get_right()->get_func_id());
		}
	}
	else
	{
		func_name_1 = (get_id_1() == ZERO_ID) ? (inv[RIGHT] && !without_inv ? "1" : "0") : (inv[RIGHT] && !without_inv ? "0" : "1");
	}
	if (op == '+' || op == '*')
	{
		return func_name_0 + op + func_name_1;
	}
	else if (op == '=')
	{
		return func_name_0;
	}
	else if (op == '^')
	{
		return '^' + func_name_0;
	}
	else if (op == '%')
	{
		if (inv[LEFT] == inv[RIGHT])
		{
			return '^' + func_name_0 + '*' + func_name_1 + '+' + func_name_0 + '*' + '^' + func_name_1;
		}
		else
		{
			return func_name_0 + '*' + func_name_1 + '+' + '^' + func_name_0 + '*' + '^' + func_name_1;
		}
	}
	else if (op == '@')
	{
		if (inv[LEFT] != inv[RIGHT])
		{
			return '^' + func_name_0 + '*' + func_name_1 + '+' + func_name_0 + '*' + '^' + func_name_1;
		}
		else
		{
			return func_name_0 + '*' + func_name_1 + '+' + '^' + func_name_0 + '*' + '^' + func_name_1;
		}
	}
	else
	{
		if (get_leaf_type() & ONE)
		{
			return "1";
		}
		else if (get_leaf_type() & ZERO)
		{
			return "0";
		}
		else if (get_leaf_type() & INPUT)
		{
			return name_by_id(get_func_id());
		}
	}
	printf("leaf.cpp:1282 :: Error case\n");
	return "Error case";
}
std::string leaf::get_inv_equal(std::string(*name_by_id) (ID_TYPE id))
{
	std::string func_name_res = is_output() ? name_by_id(func_id) : "TN_" + std::to_string(func_id);
	std::string func_name_0;
	std::string func_name_1;
	bool without_inv = (op == '@' || op == '%');
	if (get_left() != nullptr)
	{
		if (get_left()->get_leaf_type() & ZERO)
		{
			func_name_0 = !inv[LEFT] && !without_inv ? "1" : "0";
		}
		else if (get_left()->get_leaf_type() & ONE)
		{
			func_name_0 = !inv[LEFT] && !without_inv ? "0" : "1";
		}
		else
		{
			func_name_0 = (!inv[LEFT] && !without_inv) ? "^" + name_by_id(get_left()->get_func_id()) : name_by_id(get_left()->get_func_id());
		}
	}
	else
	{
		func_name_0 = (get_id_0() == ZERO_ID) ? (!inv[LEFT] && !without_inv ? "1" : "0") : (!inv[LEFT] && !without_inv ? "0" : "1");
	}
	if (get_right() != nullptr)
	{
		if (get_right()->get_leaf_type() & ZERO)
		{
			func_name_1 = !inv[RIGHT] && !without_inv ? "1" : "0";
		}
		else if (get_right()->get_leaf_type() & ONE)
		{
			func_name_1 = !inv[RIGHT] && !without_inv ? "0" : "1";
		}
		else
		{
			func_name_1 = !inv[RIGHT] && !without_inv ? "^" + name_by_id(get_right()->get_func_id()) : name_by_id(get_right()->get_func_id());
		}
	}
	else
	{
		func_name_1 = (get_id_1() == ZERO_ID) ? (!inv[RIGHT] && !without_inv ? "1" : "0") : (!inv[RIGHT] && !without_inv ? "0" : "1");
	}
	if (op == '+' || op == '*')
	{
		char inv_op = op == '+' ? '*' : '+';
		return func_name_0 + inv_op + func_name_1;
	}
	else if (op == '=')
	{
		return '^' + func_name_0;
	}
	else if (op == '^')
	{
		return func_name_0;
	}
	else if (op == '@')
	{
		if (inv[LEFT] == inv[RIGHT])
		{
			return '^' + func_name_0 + '*' + func_name_1 + '+' + func_name_0 + '*' + '^' + func_name_1;
		}
		else
		{
			return func_name_0 + '*' + func_name_1 + '+' + '^' + func_name_0 + '*' + '^' + func_name_1;
		}
	}
	else if (op == '%')
	{
		if (inv[LEFT] != inv[RIGHT])
		{
			return '^' + func_name_0 + '*' + func_name_1 + '+' + func_name_0 + '*' + '^' + func_name_1;
		}
		else
		{
			return func_name_0 + '*' + func_name_1 + '+' + '^' + func_name_0 + '*' + '^' + func_name_1;
		}
	}
	else
	{
		if (get_leaf_type() & ONE)
		{
			return "0";
		}
		else if (get_leaf_type() & ZERO)
		{
			return "1";
		}
		else if (get_leaf_type() & INPUT)
		{
			return '^' + name_by_id(get_func_id());
		}
	}
	printf("leaf.cpp:1379 :: Error case\n");
	return "Error case";
}
bool leaf::is_it_equal(base_node* r)
{
	leaf* tmp_leaf = dynamic_cast<leaf*> (r);
	if (tmp_leaf == nullptr)
	{
		// TODO: how to compare with DNF?
		return false;
	}

	ID_TYPE min_l;
	ID_TYPE max_l;
	bool min_inv_l;
	bool max_inv_l;
	ID_TYPE min_r;
	ID_TYPE max_r;
	bool min_inv_r;
	bool max_inv_r;
	ID_TYPE id0 = get_id_0();
	ID_TYPE id1 = get_id_1();
	if (id0 < id1)
	{
		min_l = id0;
		max_l = id1;
		min_inv_l = inv[LEFT];
		max_inv_l = inv[RIGHT];
	}
	else
	{
		min_l = id1;
		max_l = id0;
		min_inv_l = inv[RIGHT];
		max_inv_l = inv[LEFT];
	}
	id0 = tmp_leaf->get_id_0();
	id1 = tmp_leaf->get_id_1();
	if (id0 < id1)
	{
		min_r = id0;
		max_r = id1;
		min_inv_r = r->inv[LEFT];
		max_inv_r = r->inv[RIGHT];
	}
	else
	{
		min_r = id1;
		max_r = id0;
		min_inv_r = r->inv[RIGHT];
		max_inv_r = r->inv[LEFT];
	}
	if (min_l == min_r)
	{
		if (max_l == max_r)
		{
			if (op == tmp_leaf->get_op())
			{
				if ( (min_inv_l == min_inv_r) && (max_inv_l == max_inv_r))
				{
					return true;
				}
			}
		}
	}
	return false;
}
bool leaf::is_inv_equal(base_node* r)
{
	leaf* tmp_leaf = dynamic_cast<leaf*> (r);
	if (tmp_leaf == nullptr)
	{
		// TODO: how to compare with DNF?
		return false;
	}

	ID_TYPE min_l;
	ID_TYPE max_l;
	bool min_inv_l;
	bool max_inv_l;
	ID_TYPE min_r;
	ID_TYPE max_r;
	bool min_inv_r;
	bool max_inv_r;
	bool xor1 = false;
	char r_inv_op;
	if (tmp_leaf->get_op() == '*')
	{
		r_inv_op = '+';
	}
	else if (tmp_leaf->get_op() == '+')
	{
		r_inv_op = '*';
	}
	else if (tmp_leaf->get_op() == '@')
	{
		r_inv_op = '%';
		xor1 = true;
	}
	else if (tmp_leaf->get_op() == '%')
	{
		r_inv_op = '@';
		xor1 = true;
	}
	else
	{
		r_inv_op = tmp_leaf->get_op();
	}
	if (get_id_0() < get_id_1())
	{
		min_l = get_id_0();
		max_l = get_id_1();
		min_inv_l = inv[LEFT];
		max_inv_l = inv[RIGHT];
	}
	else
	{
		min_l = get_id_1();
		max_l = get_id_0();
		min_inv_l = inv[RIGHT];
		max_inv_l = inv[LEFT];
	}
	if (tmp_leaf->get_id_0() < tmp_leaf->get_id_1())
	{
		min_r = tmp_leaf->get_id_0();
		max_r = tmp_leaf->get_id_1();
		min_inv_r = r->inv[LEFT];
		max_inv_r = r->inv[RIGHT];
	}
	else
	{
		min_r = tmp_leaf->get_id_1();
		max_r = tmp_leaf->get_id_0();
		min_inv_r = r->inv[RIGHT];
		max_inv_r = r->inv[LEFT];
	}
	if (min_l == min_r)
	{
		if (max_l == max_r)
		{
			if (xor1)
			{

				if (op == r_inv_op)
				{
					if ((min_inv_l == min_inv_r) == (max_inv_l == max_inv_r))
					{
						return true;
					}
				}
				else if (op == tmp_leaf->get_op())
				{
					if ((min_inv_l == min_inv_r) != (max_inv_l == max_inv_r))
					{
						return true;
					}
				}
			}
			else
			{
				if (op == r_inv_op)
				{
					if ((min_inv_l != min_inv_r) && (max_inv_l != max_inv_r))
					{
						return true;
					}
				}
			}
		}
	}
	return false;
}

ID_TYPE leaf::get_id_0()
{
	if (items_beneath_cnt > 0)
		return get_left()->get_func_id();
	else
	{
		if (get_leaf_type() & ZERO) return ZERO_ID;
		if (get_leaf_type() & ONE)  return ONE_ID;
		return VOID_ID;
	}
}
ID_TYPE leaf::get_id_1()
{
	if (items_beneath_cnt > 1)
		return get_right()->get_func_id();
	else
		return VOID_ID;
}

base_node * leaf::get_left()
{
	if (items_beneath_cnt == 0) return nullptr;
	if (items_beneath_cnt > 2)
	{
		printf("[Error: get_left()]: looks like get_left() has been called for DNF table node\n");
		return nullptr;
	}
	return linked_down_array[LEFT];
}

base_node* leaf::get_right()
{
	if (items_beneath_cnt < 2) return nullptr;
	if (items_beneath_cnt > 2)
	{
		printf("[Error: get_right()]: looks like get_right() has been called for DNF table node\n");
		return nullptr;
	}
	return linked_down_array[RIGHT];
}

char leaf::get_op()
{
	return op;
}
