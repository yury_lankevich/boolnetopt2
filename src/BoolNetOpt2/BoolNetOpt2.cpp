// BoolNetOpt2.cpp: ���������� ����� ����� ��� ����������� ����������.
//
//#define _CRTDBG_MAP_ALLOC
//#include <cstdlib>
//#include <crtdbg.h>
//#define new new( _NORMAL_BLOCK, __FILE__, __LINE__)

#define SHORT_ID

#include <string>
#include <chrono>
#include <ctime>
#include "cmd_line.h"
#include <stdio.h>
#include "sf.h"
#include <chrono>
#include <ctime>

#include "boolnet_processor.h"

unsigned int boolnet_equals_organizer::tmp_cnt = 0;

std::map<std::string, ID_TYPE>* boolnet::names_map;
std::map<ID_TYPE, std::string>* boolnet::ids_map;
unsigned char                   boolnet::type = 1;
unsigned char                   k_function::type = 1;

//std::set<base_node*> base_node::all_nodes;
ID_TYPE                base_node::tmp_cnt = 0;
bool                   base_node::without_xor = false;

int main(int pcnt, char * pstr[])
{
	//_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
	//_CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDOUT);
	//_CrtMemState _ms;
	//_CrtMemCheckpoint(&_ms);
	{
		std::chrono::time_point<std::chrono::system_clock> start, end, cur;
		start = std::chrono::system_clock::now();
		cmd_line cmdline_h(pcnt, pstr);
		sf sf0(cmdline_h.param.input_file);
		boolnet_processor blnt_pr_h(sf0, cmdline_h.param);
		{
			/*
			std::string res_name = cmdline_h.param.output_file;
			res_name  = res_name + "_init" + ".sf";
			result_equals = net->convert2string();
			sf0.SaveSf((char*)res_name.data(), result_equals);
			*/
			//printf("=====================INIT DONE====================\n");

			unsigned int iteration = 0;
			bool check_iter_threshold = cmdline_h.param.max_iter > 0;
			bool check_time_threshold = cmdline_h.param.max_time_minutes > 0;
			do
			{
				if (blnt_pr_h.get_result())
				{
					sf0.SaveSf(cmdline_h.param.output_file, blnt_pr_h.result_equals);
				}
				if (check_iter_threshold)
				{
					if (++iteration >= cmdline_h.param.max_iter) break;
				}
				if (check_time_threshold)
				{
					cur = std::chrono::system_clock::now();
					std::chrono::duration<double> elapsed_time = cur - start;
					if (elapsed_time.count() / 60 >= cmdline_h.param.max_time_minutes) break;
				}
			} while (!blnt_pr_h.get_done());
		}
		delete[] cmdline_h.param.input_file;
		delete[] cmdline_h.param.output_file;
		if (cmdline_h.param.targini_isSet)
		{
			FILE* tagini_file;
			fopen_s(&tagini_file, cmdline_h.param.tagini_file, "w");
			fprintf(tagini_file, "[%s]\n", sf0.name.data());
			fprintf(tagini_file, "BDD = 0\nBDDI = 0\nENLARG = 0\nFACT_FORM = 0\nFACT_MIN = 0\nMIN = 0\nRTL = 1\nTAB = 0\n");
			fclose(tagini_file);
			delete[] cmdline_h.param.tagini_file;
		}
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = end - start;
		printf("\nElapsed time: %lf\n", elapsed_seconds.count());

	}
	//_CrtMemDumpAllObjectsSince(&_ms);

    return 0;
}