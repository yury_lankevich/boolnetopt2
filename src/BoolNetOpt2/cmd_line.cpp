#include "cmd_line.h"


input_param cmd_line::commandLine(int pcnt,char * pstr[])
{
	input_param param_;
	TCLAP::CmdLine cmd("The program is for BDD optimization", ' ', "0.1"); 
	//TCLAP::UnlabeledValueArg<std::string>  INPUT("input", "Input SF file.", true, "", "path to SF file"); 
	TCLAP::ValueArg<std::string> INPUT ("i","input" ,"Input SF file."  ,true ,"", "path to SF file"); 
	TCLAP::ValueArg<std::string> OUTPUT("o","output","Output directory ",false,"path","string");
	TCLAP::ValueArg<std::string> TAGINI("n", "tagini", "Target INI name ", false, "file", "string");
	TCLAP::ValueArg<std::string> OPTINI("r", "optionini", "Option INI name ", false, "file", "string");
	TCLAP::ValueArg<std::string> ORDER ("d", "order", "Build BoolNet with specific order", false, "order", "string");
	TCLAP::ValueArg<int> TYPE("t", "type", "Type of optimization (1-3; XOR: 1-4) ", false, 1, "int");
	TCLAP::ValueArg<int> ITER("e", "iter", "Max iteration threshold", false, 0, "int");
	TCLAP::ValueArg<int> TIME("m", "time", "Max time threshold", false, 0, "int");
	//TCLAP::SwitchArg VERBOSITY("V","verbosity","Additional output of debug data",false);
	TCLAP::SwitchArg XOR("x","xor","Use XOR",false);
	TCLAP::SwitchArg REDUCTION("c", "reduction", "Reduction only", false);
    // �������������� �������� ���������� ��������� ������ ���� ��������� � ������� ��������� ������
	cmd.add( INPUT ); 
	cmd.add( OUTPUT ); 
	//cmd.add( VERBOSITY ); 
	cmd.add(TAGINI);
	cmd.add(OPTINI);
	cmd.add(TYPE);
	cmd.add(XOR);
	cmd.add(ITER);
	cmd.add(TIME);
	cmd.add(ORDER);
	cmd.add(REDUCTION);

    cmd.parse(pcnt,pstr);  
	param_.input_file = new char[INPUT.getValue().length()+1];
	strcpy_s((param_.input_file), INPUT.getValue().length() + 1, INPUT.getValue().data());
	param_.verbosity = 0;//VERBOSITY.getValue();

	param_.type = TYPE.getValue();
	param_.xor_ = XOR.getValue();
	param_.max_iter = ITER.getValue();
	param_.max_time_minutes = TIME.getValue();
	if (ORDER.isSet()) {
		param_.order = new char[ORDER.getValue().length() + 1];
		strcpy_s(param_.order, ORDER.getValue().length() + 1, ORDER.getValue().data());
	}
	else
	{
		param_.order = nullptr;
	}
	if (REDUCTION.isSet())
	{
		param_.reduction_only = true;
	}
	else
	{
		param_.reduction_only = false;
	}
	if (OPTINI.isSet())
	{
		std::ifstream optini;
		std::string   str;
		std::vector<std::string> oResult;
		bool boolnet_opt = false;
		optini.open(OPTINI.getValue().data());

		if (optini.is_open())
		{
			while (!optini.eof())
			{
				getline(optini, str, '\n');
				CTokenizer<>::Tokenize(oResult, str);
				if (oResult.size() && oResult[0][0] == '[')
				{
					if (oResult.size() && oResult[0] == "[BOOLNET_OPT]")
					{
						boolnet_opt = true;
					}
					else
					{
						boolnet_opt = false;
					}
				}
				else if(boolnet_opt)
				{
					std::string tmp_str;
					std::vector<std::string> oResult1;
					// Join all peaces without spaces
					for (auto &i : oResult)
					{
						tmp_str = tmp_str + i;
					}
					CTokenizer<CIsChar<'='>>::Tokenize(oResult1, tmp_str);
					if (oResult1.size() >= 2)
					{
						oResult = oResult1;
					}
					if (oResult.size() && oResult[0] == "xor")
					{
						if (oResult.size() == 2)
						{
							if (oResult[1] == "1")
							{
								param_.xor_ = true;
							}
						}
						else
						{
							printf("Error in OPTINI file: in section xor of [BOOLNET_OPT]\n");
						}
						if (!param_.xor_)
						{
							if (XOR.isSet())
							{
								printf("Warning: xor section has been found in OPTINI file, key -x will be ignored\n");
							}
						}
					}
					else if (oResult.size() && oResult[0] == "type")
					{
						if (oResult.size() == 2)
						{
							param_.type = std::stoi(oResult[1]);
						}
						else
						{
							printf("Error in OPTINI file: in section type of [BOOLNET_OPT]\n");
						}
						if (TYPE.isSet())
						{
							printf("Warning: type section has been found in OPTINI file, key -t will be ignored\n");
						}
					}
					else if (oResult.size() && oResult[0] == "time_threshold")
					{
						if (oResult.size() == 2)
						{
							param_.max_time_minutes = std::stoi(oResult[1]);
						}
						else
						{
							printf("Error in OPTINI file: in section type of [BOOLNET_OPT]\n");
						}
						if (TIME.isSet())
						{
							printf("Warning: time_threshold section has been found in OPTINI file, key -m will be ignored\n");
						}
					}
					else if (oResult.size() && oResult[0] == "iter_threshold")
					{
						if (oResult.size() == 2)
						{
							param_.max_iter = std::stoi(oResult[1]);
						}
						else
						{
							printf("Error in OPTINI file: in section type of [BOOLNET_OPT]\n");
						}
						if (ITER.isSet())
						{
							printf("Warning: iter_threshold section has been found in OPTINI file, key -m will be ignored\n");
						}
					}
					else if (oResult.size() && oResult[0] == "reduction_only")
					{
						if (oResult.size() == 2)
						{
							param_.reduction_only = std::stoi(oResult[1]);
						}
						else
						{
							printf("Error in OPTINI file: in section type of [BOOLNET_OPT]\n");
						}
						if (ITER.isSet())
						{
							printf("Warning: reduction_only section has been found in OPTINI file, key -c will be ignored\n");
						}
					}
				}
			}
		}
		optini.close();
	}

	if(OUTPUT.isSet()){
		param_.output_file = new char [OUTPUT.getValue().length()+1];
		strcpy_s(param_.output_file, OUTPUT.getValue().length() + 1, OUTPUT.getValue().data());
	}
	else
	{
		param_.output_file = new char [8];
		strcpy_s(param_.output_file, 8, "demo.sf");
	}

	if (TAGINI.isSet())
	{
		param_.tagini_file = new char[TAGINI.getValue().length() + 1];
		strcpy_s(param_.tagini_file, TAGINI.getValue().length() + 1, TAGINI.getValue().data());
		param_.targini_isSet = true;
	}
	else
	{
		param_.targini_isSet = false;
	}
	return param_;
}

cmd_line::cmd_line(int pcnt,char * pstr[])
{
	param = commandLine(pcnt, pstr);
}

cmd_line::~cmd_line()
{
	//delete [] param.output_file;
}