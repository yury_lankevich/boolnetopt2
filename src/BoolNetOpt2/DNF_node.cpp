#include "DNF_node.h"

base_node* DNF_node::get_instance_copy(ID_TYPE func_id_)
{
	DNF_node* nd = new DNF_node(func_id_, status);
	nd->k_func = k_func;
	return nd;
}

DNF_node::DNF_node(ID_TYPE func_id_, unsigned char status_) : base_node(func_id_, status_)
{
}

void DNF_node::update()
{
	if (lock_update || (get_leaf_type() & (EXCLUDED | ZERO | ONE | INPUT)))
		return;
	{
		std::vector<unsigned int> positions;
		std::vector<char> codes;
		for (int i = items_beneath_cnt - 1; i >= 0; i--)
		{
			if (linked_down_array[i]->get_leaf_type() & ONE)
			{
				positions.push_back(i);
				codes.push_back(0x2);
			}
			else if (linked_down_array[i]->get_leaf_type() & ZERO)
			{
				positions.push_back(i);
				codes.push_back(0x1);
			}
		}
		if (positions.size() > 0)
		{
			k_func.exclude_pos(positions, codes);
			// Update linked_down_array
			for (auto& i : positions)
			{
				linked_down_array[i]->delete_linked_up(this);
				linked_down_array[i] = nullptr;
			}
			int vac_pos = positions.back();
			for (unsigned int i = vac_pos; i < items_beneath_cnt; i++)
			{
				if (linked_down_array[i] != nullptr)
				{
					linked_down_array[vac_pos++] = linked_down_array[i];
					linked_down_array[i] = nullptr;
				}
			}
			items_beneath_cnt = items_beneath_cnt - (unsigned int) positions.size();
		}
	}

	k_func.minimization();

	if (k_func.size() == 0) // ZERO
	{
		set_leaf_type(ZERO);
	}
	else
	{
		big_number descr = k_func.get_descriptor();
		if (descr.is_one()) // ONE
		{
			set_leaf_type(ONE);
		}
		else
		{
			std::vector<unsigned int> positions;
			for (int i = items_beneath_cnt - 1; i >= 0; i--)
			{
				if (descr.get_code(i) == 0x3)
				{
					positions.push_back(i);
				}
			}
			if (positions.size() > 0)
			{
				k_func.exclude_pos(positions);
				// Update linked_down_array
				for (auto& i : positions)
				{
					linked_down_array[i]->delete_linked_up(this);
					linked_down_array[i] = nullptr;
				}
				int vac_pos = positions.back();
				for (unsigned int i = vac_pos; i < items_beneath_cnt; i++)
				{
					if (linked_down_array[i] != nullptr)
					{
						linked_down_array[vac_pos++] = linked_down_array[i];
						linked_down_array[i] = nullptr;
					}
				}
				items_beneath_cnt = items_beneath_cnt - (unsigned int) positions.size();
			}
		}
	}
	if (get_leaf_type() & (ONE | ZERO) && linked_down_array_size != 0)
	{
		for (unsigned int i = 0; i < items_beneath_cnt; i++)
		{
			linked_down_array[i]->delete_linked_up(this);
		}
		items_beneath_cnt = 0;
		delete[] linked_down_array;
		linked_down_array_size = 0;
	}
}

void DNF_node::set_DNF(std::set<big_number>& DNF_set, std::map<base_node*, int>& DNF_map)
{
	k_func.add_conjunction(DNF_set);
	// Update items beneath
	if (linked_down_array_size != DNF_map.size() && linked_down_array_size > 0)
	{
		delete[] linked_down_array;
	}
	linked_down_array_size = (unsigned int) DNF_map.size();
	items_beneath_cnt = (unsigned int) DNF_map.size();
	linked_down_array = new base_node * [linked_down_array_size];
	inv = new bool[linked_down_array_size];
	for (auto& i : DNF_map)
	{
		linked_down_array[i.second] = i.first;
		i.first->add_linked_up(this);
	}
	update();
}
