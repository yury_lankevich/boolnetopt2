
//Tokenizer.h

#ifndef __TOKENIZER_H__
#define __TOKENIZER_H__

#pragma warning(disable:4786)

#include <functional>
#include <string>
#include <vector>
#include <set>
#include <algorithm>

//using namespace std;

//For the case the default is a space.
//This is the default predicate for the Tokenize() function.

template <const char tch=' '>
class CIsChar : public std::function<bool(char)>
{
public:
  bool operator()(char c) const;
};

template <const char tch>
inline bool CIsChar<tch>::operator()(char c) const
{

  //isspace<char> returns true if c is a white-space character (0x09-0x0D or 0x20)
  return (tch == c);
}

//For the case the separator is a comma
class CIsComma : public std::function<bool(char)>
{
public:
  bool operator()(char c) const;
};

inline bool CIsComma::operator()(char c) const
{
  return (',' == c);
}

//For the case the separator is a character from a set of characters given in a string
class CIsFromString : public std::function<bool(char)>
{
public:
  //Constructor specifying the separators
  CIsFromString(std::string const& rostr) : m_ostr(rostr) {}
  bool operator()(char c) const;

private:
  std::string m_ostr;
};

inline bool CIsFromString::operator()(char c) const
{
  std::size_t iFind = m_ostr.find(c);
  if(iFind != std::string::npos)
    return true;
  else
    return false;
}

//String Tokenizer
template <class Pred=CIsChar<>>
class CTokenizer
{
public:
  //The predicate should evaluate to true when applied to a separator.
  static void Tokenize(std::vector<std::string>& roResult, std::string const& rostr, Pred const& roPred=Pred(), bool fc = true);
};

//The predicate should evaluate to true when applied to a separator.
template <class Pred>
inline void CTokenizer<Pred>::Tokenize(std::vector<std::string>& roResult, std::string const& rostr, Pred const& roPred, bool fc)
{
	if (fc)
  //First clear the results vector
	roResult.clear();
  std::string::const_iterator it = rostr.begin();
  std::string::const_iterator itTokenEnd = it;
  while(it != rostr.end())
  {
	  //Eat seperators
            // Additional check for end of string here, needed if the last character(s)
            // in the input string are actually separator characters themselves.
            // Enables handling of a string composed entirely of separators [TPK 04-10-06]
            while( it != rostr.end() && roPred( *it ) )
            {
                  it++;
            }
    //Find next token
    itTokenEnd = find_if(it, rostr.end(), roPred);
    //Append token to result
    if(it < itTokenEnd)
      roResult.push_back(std::string(it, itTokenEnd));
    it = itTokenEnd;
  }
}

#endif //__TOKENIZER_H__

