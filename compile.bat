set path=c:\"Program Files"\mingw-w64\x86_64-8.1.0-win32-seh-rt_v6-rev0\mingw64\bin
set CPP=%path%\cpp.exe
set GCC=%path%\gcc.exe
set GPP=%path%\g++.exe
set AS=%path%\as.exe
set INC_PATH=-I .\src\ -I .\src\tclap\ -I .\src\BoolNetOpt2\ -I .\inc\
set LIB_PATH=-L .\inc\lib\x64

%GPP% -g0 -fopenmp -O3 -static-libgcc -static-libstdc++ -std=c++14 src\BoolNetOpt2\*.cpp %INC_PATH% %LIB_PATH% -static -lgomp -l libboost_filesystem -l libboost_system -o BoolNetOpt2.exe